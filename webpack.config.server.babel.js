// Get our config
import dotenv from 'dotenv';
dotenv.config();

import webpack from 'webpack';
import path from 'path';
import WebpackNotifierPlugin from 'webpack-notifier';
import {sharedPlugins} from './webpack.shared-plugins.js';

/* SERVER CONFIG */
export default [

	{
		mode:  process.env.ENVIRONMENT,
		target: 'node',
		node: {
			__dirname: false,
			__filename: false,
		},
		devtool: 'source-map',
		entry: ['./src/server/app.js'],
		output: {
			path: path.resolve(__dirname, 'dist'),
			libraryTarget: 'commonjs',
			filename: './app.js'
		},
		// Don't compile anything in node_modules or without a relative path
		externals: [
			/^(?!\.|\/).+/i,
		],
		module: {

			rules: [

				// Babel-loader for webpack
				{
					test: /\.js$/,
					exclude: /(node_modules|bower_components)/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env'],
							plugins:
							[
								[
									'@babel/plugin-transform-runtime',
									{
										regenerator : true,
									}
								],
							],
						}
					}
				},

			]

		},
		plugins: [

			// Send a notification when the build is finished
			new WebpackNotifierPlugin({
				title: 'Server assets'
			}),

		].concat(sharedPlugins),
	}
];