# Med Andra Ord
A web-based party game

Additional sound effects from https://www.zapsplat.com

## Index
- [To-do](#to-do)
	- [General](#general)
	- [Frontend](#frontend)
	- [Database](#database)

## To-do

### General
- [X] Use React.js for frontend
- [X] Setup project structure
- [ ] Check clientside scripts for leftover debug code
- [ ] OnResizeEnd: fix wrapper height and range slider thumb position

## Frontend
- [ ] Fix header meta
- [ ] Add fullscreen button
- [ ] Add feature discovery
- [ ] Move version text to bottom of wrapper

### Database
- [X] Add class for database handling
- [X] Create function to check and correct database structure
- [ ] Rework database handling to use schemas