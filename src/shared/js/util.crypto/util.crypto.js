'use strict';

// Import our modules and classes
import crypto from 'crypto';

// Wrapper for all utility functions
const utilCrypto = {

	// Generate password hash and salt
	hashPassword(password, salt = crypto.randomBytes(16).toString('hex')) {

		password = crypto.pbkdf2Sync(
			password, 
			salt, 
			1000, 
			128, 
			'sha512'
		).toString('hex');

		return {
			hash: password,
			salt: salt
		};

	}

}

export default utilCrypto;