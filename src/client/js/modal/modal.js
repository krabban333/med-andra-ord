'use strict';

// Import our modules and classes
import $ from 'jquery';
import Swal from 'sweetalert2';
import enumElements from '../utilities/enum.elements.js';
import enumText from '../utilities/enum.text.js';
import utility from '../utilities/utilities.js';

// Import out modals
import modalLogin from './modal.login.js';
import modalLogout from './modal.logout.js';
import modalSuccess from './modal.success.js';
import modalConfirmEndgame from './modal.confirm-endgame.js';

export default class Modal {

	constructor() {

		// Available modals
		this.modalLogin = modalLogin;
		this.modalLogout = modalLogout;
		this.modalSuccess = modalSuccess;
		this.modalConfirmEndgame = modalConfirmEndgame;

		// Our callbacks
		this.submitCallback;
		this.cancelCallback

		// Variable used to pass data between modal functions
		this.callbackData;

		// Current modal
		this.modal;

	}

	// Checks if the modal is visible
	isVisible() {

		return Swal.isVisible();

	}

	// Create a new Sweetalert2 modal
	createModal(type, submitCallback = {}, cancelCallback = {}) {

		// Bind callbacks
		this.submitCallback = submitCallback;
		this.cancelCallback = cancelCallback;

		// Reset callback data
		this.callbackData = {};

		// Bind correct modal type
		this.modal = this[type];

		// Set default options
		this.modal.stopKeydownPropagation = false;
		this.modal.allowOutsideClick = () => !Swal.isLoading();

		// Bind default onOpen function if not already set
		if(this.modal.onOpen === undefined) {

			this.modal.onOpen = () => {

				let $modalElement = $(enumElements.modal.classes),
						$inputElements = $modalElement.find('input, textarea, button');

				// Add tabindex to all inputs
				$.each($inputElements, (index, element) => {

					let $element = $(element);

					$element.attr('tabindex', index);

				});

				// Blur any focused inputs
				$inputElements.blur();

			}

		}

		// Create modal
		Swal(this.modal)
		.then((resolve) => {

			// On submit
			if(resolve.value) {

				this._callback(this.submitCallback);

			// On cancel
			} else {

				this._callback(this.cancelCallback);

			}

		})
		.catch((err) => {

			console.log('error:', err);
			
			Swal.close();

		});

	}

	// Select modal action
	selectAction(action) {

		switch(action) {

			// Submit modal
			case 'submit':

				let valid = this._validateInput();

				// Run preconfirm function if it is set
				if(valid && typeof this.modal.preConfirm === 'function') {

					this._doPreconfirm();

				} else {

					// Validate the modal inputs and submit if everything is ok
					(valid && Swal.clickConfirm());

				}

				break;

			// Cancel modal
			case 'cancel':
			default:

				Swal.clickCancel();

				break;

		}

	}

	// Handle keydown event inside modal
	keydownHandler(event) {

		let $modalElement = $(enumElements.modal.classes),
				$elements = {
					input: $modalElement.find('input'),
					textarea: $modalElement.find('textarea'),
					button: $modalElement.find('button'),
					all: $modalElement.find('input, textarea, button'),
				},
				isTab = (event.which === 9 ? true : false),
				isEnter = (event.which === 13 ? true : false);

		// Check if any element has focus
		if(
			$elements.input.filter(':focus').length === 0
			&&
			$elements.textarea.filter(':focus').length === 0
			&&
			$elements.button.filter(':focus').length === 0
		) {

			// Check if pressed key was tab
			if(isTab) {

				event.preventDefault();

			}

			// Focus first element
			for(let element in $elements) {

				// We only care about visible elements
				let $element = $modalElement.find(element).filter(':visible');

				if($element.length > 0) {

					$element.first()
					.focus();

					break;

				}

			}

		// Input or textarea element is focused
		} else if(
			$elements.input.filter(':focus').length > 0
			||
			$elements.textarea.filter(':focus').length > 0
		) {

			// Check if pressed key was enter
			if(isEnter) {

				let $visibleElements = $elements.all.filter(':visible'),
						tabindex = $elements.all.filter(':focus').attr('tabindex'),
						array = [];

				event.preventDefault();

				// Map visible elements to array
				array = $visibleElements.map((index, key) => {

					return {
						tabindex: parseInt($(key).attr('tabindex')),
						object: $(key),
					};

				});

				// Sort elements by tabindex in descending order
				array.sort((a, b) => {

					return a.tabindex - b.tabindex;

				});

				// Loop through each element
				$.each(array, (index, value) => {

					// Is the elements tabindex greater then the currently focused element
					if(value.tabindex > tabindex) {

						// Focus this element
						value.object.focus();

						// Is the next element the submit button?
						if(value.object.filter('[data-action="submit"]').length > 0) {

							// Trigger click
							value.object.trigger('click');

						}

						// Stop the loop
						return false;

					}

				});

			}

		}

	}

	// Run preconfirm function
	_doPreconfirm() {

		let $modalElement = $(enumElements.modal.classes),
				$footerButtons = $modalElement.find('.swal2-footer button'),
				$inputElements = $modalElement.find('input, textarea');

		// Show loading
		Swal.showLoading();

		// Disable user input elements
		$footerButtons.addClass('disabled');
		$inputElements.attr('disabled', true);

		// Run preconfirm function
		this.modal.preConfirm()
		.then((result) => {

			if(result.status) {

				// Run submit callback
				this._callback(this.submitCallback);

				// Close modal
				Swal.close();

			} else {

				// Reset values on inputs of type password
				$inputElements.filter('[type="password"]').val('');

				// Enable user input elements
				$footerButtons.removeClass('disabled');
				$inputElements.attr('disabled', false);

				// Show error
				Swal.showValidationMessage(result.message);

				// Hide loading
				Swal.hideLoading();

			}

		});

	}

	// Validate modal inputs
	_validateInput() {

		let $modalElement = $(enumElements.modal.classes),
				$elements = $modalElement.find('input, textarea').filter(':visible'),
				$errorElement = $(Swal.getValidationMessage()),
				inner,
				status = true;

		// Add inner element
		$errorElement.html('<span class="inner"></span>');

		// Get inner element
		inner = $errorElement.find('.inner');

		// Loop through each input element
		$.each($elements, (index, element) => {

			let $element = $(element),
					name = $element.siblings('label').text().trim().toLowerCase(),
					value = $element.val();

			// Check if value is set
			if(!utility.isSet(value)) {

				// Set error message
				$(inner).text(`${enumText.input.empty} ${name}`);

				// Is error element visible
				if($errorElement.css('display') != 'block') {

					// Animate error element
					$errorElement.css({
						display: 'block',
						opacity: 0,
					})
					.addClass('fade-in');

				// Animate error text
				} else {

					utility.refreshAnimation(inner, 'fade-in');

				}

				status = false;

				return false;

			} else {

				this.callbackData[element.id] = element.value;

			}

		});

		(status && $errorElement.css({display: 'none'}));

		return status;

	}

	// Run callback
	_callback(callback) {

		if(typeof callback === 'function') {

			callback(this.callbackData);

		}

	}

}