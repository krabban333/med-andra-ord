'use strict';

// Import our modules and classes
import enumText from '../utilities/enum.text.js';
import utility from '../utilities/utilities.js';

// Modal footer
let footer = `
<div class="wrapper">
	<button type="button" class="btn waves-effect waves-light reverse" data-action="submit">
		${enumText.modal.logout.submit}
	</button>
	<button type="button" class="btn cancel reverse" data-action="cancel">
		${enumText.modal.cancel}
	</button>
</div>`;

// Modal
const modalLogout = {
	titleText: enumText.modal.logout.title,
	footer: footer,
	animation: true,
	showConfirmButton: false,
	showCancelButton: false,
}

export default modalLogout;