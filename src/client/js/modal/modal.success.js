'use strict';

// Modal
const modalSuccess = {
	type: 'success',
	animation: true,
	showConfirmButton: false,
	showCancelButton: false,
	showCloseButton: true,
	timer: 1050,
}

export default modalSuccess;