'use strict';

// Import our modules and classes
import enumText from '../utilities/enum.text.js';
import utility from '../utilities/utilities.js';

// Modal footer
let footer = `
<div class="wrapper">
	<button type="button" class="btn waves-effect waves-light reverse" data-action="submit">
		${enumText.modal.confirmEndgame.submit}
	</button>
	<button type="button" class="btn cancel reverse" data-action="cancel">
		${enumText.modal.cancel}
	</button>
</div>`;

// Modal
const modalConfirmEndgame = {
	titleText: enumText.modal.confirmEndgame.title,
	footer: footer,
	animation: true,
	showConfirmButton: false,
	showCancelButton: false,
}

export default modalConfirmEndgame;