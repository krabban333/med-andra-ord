'use strict';

// Import our modules and classes
import enumElements from '../utilities/enum.elements.js';
import enumText from '../utilities/enum.text.js';
import utility from '../utilities/utilities.js';

let modalUsername = enumElements.noPrefix(enumElements.modal.input.modalUsername.id),
		modalPassword = enumElements.noPrefix(enumElements.modal.input.modalPassword.id);

// Custom modal html
let html = `
<div class="input-field">
	<input id=${modalUsername} type="text" maxlength="18" autocomplete="off" />
	<label for=${modalUsername}>
		${enumText.modal.login.username}
	</label>
</div>
<div class="input-field">
	<input id=${modalPassword} type="password" maxlength="18" />
	<label for=${modalPassword}>
		${enumText.modal.login.password}
	</label>
</div>`;

// Modal footer
let footer = `
<div class="wrapper reverse">
	<button type="button" class="btn waves-effect waves-light" data-action="submit">
		${enumText.modal.login.submit}
	</button>
	<button type="button" class="btn cancel" data-action="cancel">
		${enumText.modal.cancel}
	</button>
</div>`;

// Modal
const modalLogin = {
	titleText: enumText.modal.login.title,
	html: html,
	footer: footer,
	animation: true,
	showConfirmButton: false,
	showCancelButton: false,

	preConfirm: () => {

		return new Promise((resolve, reject) => {

			// Get input values
			let time = Date.now(),
					delay = 500,
					data = {
				username: document.getElementById(modalUsername).value.toLowerCase(),
				password: document.getElementById(modalPassword).value,
			};

			// Send data to server
			utility.ajaxPOST(
				'/auth',
				data
			)
			.always((response) => {

				let status = (response ? true : false),
						result;

				// Create result object
				result = {
					status: status,
					message: (!status ? enumText.input.userDenied : ''),
				};

				// Set minimum delay before resolve
				if(Date.now() - time < delay) {

					setTimeout(() => {

						resolve(result);

					}, delay - (Date.now() - time));

				} else {

					resolve(result);

				}

			});

		});

	}

}

export default modalLogin;