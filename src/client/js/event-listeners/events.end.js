'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';

export default class EndEvents {

	constructor(contentHandler, gameLoop, mainMenuEvents) {

		// Content handler instance
		this.content = contentHandler;

		// Game loop handler
		this.game = gameLoop;

		// Main menu event handler
		this.mainMenuEvents = mainMenuEvents;

		this._events();

	}

	// Event listeners
	_events() {

		let end = enumElements.wrapper.end;

		// On animation end for last list element
		$(document).on(
			'animationend webkitAnimationEnd oAnimationEnd',
			`${end.id} .last`,
			() => {

				let $endWrapper = $(end.id);

				// Restore wrapper height when animation has finished
				$endWrapper.css({height: 'auto'})

			}
		);

		// On main menu button click
		$(document).on(
			'click',
			`${end.id} ${end.buttons.mainMenu.id}`,
			() => {

				// Clear all game related variables
				this.game.resetGame();

				// Render header
				this.content.renderContent(
					enumElements.header.id,
					'Header'
				);

				// Render main menu
				this.content.renderContent(
					enumElements.wrapper.id,
					'MainMenu'
				);

			}
		);

		// On score button click
		$(document).on(
			'click',
			end.buttons.score.id,
			() => {

				// Display score screen
				this.game.displayScoreScreen(true);

			}
		);

		// On new game button click
		$(document).on(
			'click',
			end.buttons.newGame.id,
			() => {

				// Clear all game related variables
				this.game.resetGame();

				// Render options page
				this.content.renderContent(
					enumElements.wrapper.id,
					'GameOptions',
					undefined,
					() => this.mainMenuEvents.displayOptionsPage()
				);

			}
		);

	}

}