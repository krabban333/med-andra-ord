'use strict';

// Import our modules and classes
import $ from 'jquery';
import M from 'materialize-css';
import enumElements from '../utilities/enum.elements.js';
import enumText from '../utilities/enum.text.js';
import applicationData from '../utilities/utilities.application-data.js';
import cookieHandler from '../utilities/utilities.cookie-handler.js';
import utility from '../utilities/utilities.js';

export default class GameOptionsEvents {

	constructor(gameLoop, mediaHandler) {

		// Game loop handler
		this.game = gameLoop;

		// Media handler
		this.media = mediaHandler;

		this._events();

	}

	// Event listeners
	_events() {

		let gameOptions = enumElements.wrapper.gameOptions,
				maxLength = 18;

		// On controls button click
		$(document).on(
			'click',
			`${gameOptions.id} .controls > span:not(.disabled)`,
			(event) => {

				let $element = $(event.currentTarget);

				utility.refreshAnimation(
					$element, 
					'bounce'
				);

				// Play click sound
				this.media.playAudio(
					'click_alt',
					0.2
				);

			}
		);

		// On controls button click
		$(document).on(
			'input',
			`${gameOptions.id} input[type=range]`,
			() => {

				// Play click sound
				this.media.playAudio(
					'range_change',
					0.2
				);

			}
		);

		// On animation end for teamlist item
		$(document).on(
			'animationend webkitAnimationEnd oAnimationEnd',
			`${gameOptions.teamEditor.teamList.classes} .collection-item`,
			function() {

				let $element = $(this);

				// Remove animation class
				// This is done to prevent wonky behaviour of collapsible parent
				$element.removeClass('team-slide-down');

			}
		);

		// On animation end for round clock time
		$(document).on(
			'animationend webkitAnimationEnd oAnimationEnd',
			`${gameOptions.roundClock.id} .value span`,
			function() {

				let $element = $(this),
						time = $element.data('time'),
						unit = $element.data('unit');

				// Does the element have the animate out class
				if($element.hasClass('clock-fade-out')) {

					// Set the element text to the time
					$element.text(`${time}${unit}`);

					// Hide the element
					$element.addClass('hide');

				}

			}
		);

		// On team number input
		$(document).on(
			'input',
			`${gameOptions.teamsSlider.id} input`,
			function() {

				let $elementRange = $(this),
						$teamEditor = $(gameOptions.teamEditor.id),
						$elementTeamList = $teamEditor.find('.team-list'),
						$elementsTeams = $elementTeamList.find('li'),
						$numOfTeamsCounter = $teamEditor.find('.collapsible-header .num-of-teams > span'),
						rangeValue = $elementRange.val(),
						numberOfTeams = $elementsTeams.length;

				// Set number of teams in team editor header
				$numOfTeamsCounter.text(rangeValue);

				// Did we add teams
				if(rangeValue > numberOfTeams) {

					// Append teams to list
					for(let index = numberOfTeams; index < rangeValue; index++) {

						let id = index + 1,
								animationClass = ($teamEditor.find('li').hasClass('active') ? 'team-slide-down' : '');

						$elementTeamList.append(`<li id="team-${id}" class="collection-item ${animationClass}">
							<i class="fas fa-pen"></i>
							<span class="edit-field" contentEditable="true">${enumText.gameOptions.teamEditor.team} ${id}</span>
						</li>`);

					}

				// Did we remove teams
				} else if(rangeValue < numberOfTeams) {

					// Remove teams from list
					for(let index = numberOfTeams; index > rangeValue; index--) {

						$elementsTeams.remove(`#team-${index}`);

					}

				}

			}
		);

		// Keydown on edit team text field
		$(document).on(
			'keydown',
			`${gameOptions.teamEditor.teamList.classes} .collection-item .edit-field`,
			function(event) {

				// Handle keydown event
				utility.contentEditableKeyevent(
					event,
					this,
					maxLength,
					13,
					() => {

						event.preventDefault();

						// Select next content editable field
						$(this).blur()
						.parent('.collection-item')
						.next()
						.children('.edit-field')
						.focus();

					}
				);

			}
		);

		// Keyup on edit team text field
		$(document).on(
			'keyup',
			`${gameOptions.teamEditor.teamList.classes} .collection-item .edit-field`,
			function(event) {

				let $element = $(this),
						text = $element.text(),
						regex = RegExp(/(^[\s]|[\s]{2,})/, 'g'); // Test if string has preceding or double whitespace

				// Remove excessive whitespaces
				if(!!regex.test(text)) {

					$element.text(utility.trimWhitespaces(text));

					utility.placeCaretAtEnd($element[0]);
					
				}

			}
		);

		// On edit team icon click
		$(document).on(
			'click',
			`${gameOptions.teamEditor.teamList.classes} .collection-item svg`,
			function() {

				let $element = $(this),
						$textElement = $element.next('.edit-field');

				// Focus the correct text element
				$textElement.focus();

			}
		);

		// On edit team text field focus
		$(document).on(
			'focus',
			`${gameOptions.teamEditor.teamList.classes} .collection-item .edit-field`,
			function() {

				let $element = $(this),
						$parent = $element.parent('.collection-item');

				// Has the field been edited before
				if(!$parent.hasClass('edited')) {
					
					// Save previous text as placeholder
					$element.data({placeholder: $element.text()});

					// Clear text
					$element.text('');
					
				}

				// Set parent wrapper to active
				$parent.addClass('active');

			}
		);

		// On edit team text field blur
		$(document).on(
			'blur',
			`${gameOptions.teamEditor.teamList.classes} .collection-item .edit-field`,
			function() {

				let $element = $(this),
						$parent = $element.parent('.collection-item'),
						placeholder = $element.data('placeholder'),
						text = $element.text();

				// Remove active class from parent
				$parent.removeClass('active');

				// Check if input was empty
				if(utility.trimWhitespaces(text).length === 0) {

					// Set text to placeholder if empty
					$element.text(placeholder);

					// Remove edited class
					$parent.removeClass('edited');

				} else {

					// Add class edited
					$parent.addClass('edited');

				}

			}
		);

		// On round clock controls click
		$(document).on(
			'click',
			`${gameOptions.roundClock.id} ${gameOptions.roundClock.controls.classes} span`,
			(event) => {

				let $element = $(event.target || event.srcElement).parents('span'),
						$clock = $element.parents(gameOptions.roundClock.id),
						$plus = $clock.find('.plus'),
						$minus = $clock.find('.minus'),
						timeObj = {
							minutes: $clock.find(gameOptions.roundClock.minutes.classes),
							seconds: $clock.find(gameOptions.roundClock.seconds.classes),
						},
						min = $clock.data('min'),
						max = $clock.data('max'),
						step = $clock.data('step'),
						value = $clock.data('value'),
						action = $element.data('action'),
						doUpdate = false;

				// Select action
				switch(action) {

						// On add time
						case 'plus':

							// Does the new time exceed the max time
							if(value + step <= max) {
								
								// Add time
								value += step;
								doUpdate = true;

							}

							break;

						// On subtract time
						case 'minus':

							// Is the new time smaller then the min time
							if(value - step >= min) {
								
								// Subtract time
								value -= step;
								doUpdate = true;

							}

							break;

						default:
							console.error('Round clock: action not found');
							break;

				}

				// Save time
				$clock.data('value', value);

				// Update clock
				if(doUpdate) {

					// Loop through our time objects (minutes, seconds)
					for(let timeUnit in timeObj) {

						// Run our time animation function for each time unit
						this._clockToggleTime(
							timeObj[timeUnit],
							value
						);

					}

				}

				// Disable add button if max value is reached
				if(value === max) {

					$plus.addClass('disabled');

				} else {

					$plus.removeClass('disabled');

				}

				// Disable subtract button if min value is reached
				if(value === min) {

					$minus.addClass('disabled');

				} else {

					$minus.removeClass('disabled');

				}

			}
		);

		// On game type selection click 
		$(document).on(
			'click',
			gameOptions.gameType.help.icon.id,
			() => {

				let $iconWrapper = $(gameOptions.gameType.help.icon.id),
						$iconOpen = $iconWrapper.find('.open'),
						$iconClose = $iconWrapper.find('.close'),
						$infobox = $(gameOptions.gameType.help.infoBox.id),
						animationIn = 'slide-in',
						animationOut = 'slide-out';

				// Is the infobox displayed
				if($infobox.hasClass(animationIn)) {

					$iconOpen.css({display: 'block'});
					$iconClose.css({display: 'none'});

					// Remove animation class
					$infobox.removeClass(animationIn);

					// Hide infobox
					utility.refreshAnimation(
						$infobox[0],
						animationOut
					);

				// Is the infobox hidden
				} else {

					$iconOpen.css({display: 'none'});
					$iconClose.css({display: 'block'});

					// Remove animation class
					$infobox.removeClass(animationOut);

					// Display infobox
					utility.refreshAnimation(
						$infobox[0],
						animationIn
					);

				}

			}
		);

		// On game type selection change 
		$(document).on(
			'change',
			`${gameOptions.gameType.id} select`,
			function() {

				let $element = $(this),
						$parent = $element.parents('.card-content'),
						$tabs = $parent.find('.tab'),
						$defaultOption = $element.find('#default'),
						$selection = $element.find(':selected'),
						$helpIcon = $(gameOptions.gameType.help.icon.id),
						$iconOpen =$helpIcon.find('.open'),
						$iconClose = $helpIcon.find('.close'),
						$infoBox = $(gameOptions.gameType.help.infoBox.id),
						tabs = $selection.data('tabs').split(',');

				// Does a placeholder option exist
				if($defaultOption.length > 0) {

					// Display help icon
					$helpIcon.css({display: 'block'})
					.addClass('fade-in');

					// Remove placeholder option
					$defaultOption.remove();

					// Reinitialize Materialize select
					M.FormSelect.init($element[0]);

				}

				// Display the question mark help icon
				$iconOpen.css({display: 'block'});
				$iconClose.css({display: 'none'});

				// Set correct help infobox text and hide it
				$infoBox.text($selection.data('description'))
				.removeClass('slide-in slide-out alert')
				.css({
					padding: 0,
					height: 0,
				});

				// Push fix to bottom of queue
				setTimeout(() => {

					// Set thumb to correct position for each range element
					utility.setRangesliderThumb();

				}, 0);

				// Hide all tabs
				$tabs.css({
					display: 'none'
				});

				// Iterate through selected tabs
				$.each(
					tabs,
					(_, value) => {

						let $element = $(`.${value}`);

						// Display tabs
						$element.css({
							display: 'block'
						});

						utility.refreshAnimation(
							$element[0],
							'slide-in-alt',
						);

					}
				);

			}
		);

		// On counter component click
		$(document).on(
			'click',
			`.counter:not(${gameOptions.roundClock.id}) .controls > span`,
			function() {

				let $element = $(this),
						$counter = $element.parents('.counter'),
						$value = $counter.find('.value'),
						$plus = $counter.find('.controls .plus'),
						$minus = $counter.find('.controls .minus'),
						value = $counter.data('value'),
						min = $counter.data('min'),
						max = $counter.data('max'),
						step = $counter.data('step'),
						action = $element.data('action');

				// Select correct action
				switch(action) {

					// Add to value
					case 'plus':

						// Is value already max
						if(value + step <= max) {

							// Increment value by step
							value += step;

						} else {

							value = max;

						}

						break;

					case 'minus':

						// Is value already min
						if(value - step >= min) {

							// Decrease value by step
							value -= step;

						} else {

							value = min;

						}

						break;

					default:
						console.error('Counter: action not found');
						break;

				}

				// Clicked element is not disabled
				if(!$element.hasClass('disabled')) {

					// Set counter value and visible value
					$counter.data('value', value);
					$value.text(value);

					// Animate value
					utility.refreshAnimation(
						$value[0],
						'clock-fade-in'
					);

				}

				// Disable plus button if max is reached
				if(value === max) {

					$plus.addClass('disabled');

				// Else enable plus button
				} else {

					$plus.removeClass('disabled');

				}

				// Disable minus button if min is reached
				if(value === min) {

					$minus.addClass('disabled');

				// Else enable minus button
				} else {

					$minus.removeClass('disabled');

				}

			}
		);

		// On start game click
		$(document).on(
			'click',
			gameOptions.startGame.id,
			() => {

				let $startButton = $(enumElements.wrapper.gameOptions.startGame.id),
						$teamEditor = $(gameOptions.teamEditor.id),
						$teams = $teamEditor.find('.collapsible-body li'),
						$roundTime = $(gameOptions.roundClock.id),
						$gameType = $(gameOptions.gameType.id),
						$infobox = $gameType.siblings(gameOptions.gameType.help.infoBox.id),
						$numRounds = $(gameOptions.numRounds.id),
						$numPoints = $(gameOptions.numPoints.id),
						gameType = parseInt($gameType.find(':selected').val());

				// Is the gametype set
				if(gameType > 0) {

					let teams = [],
							roundTime = $roundTime.data('value'),
							rounds = $numRounds.data('value'),
							points = parseInt($numPoints.find('input').val());

					// Display button as loading
					$startButton.width($startButton.width())
					.attr('disabled', true)
					.html('<i class="fas fa-circle-notch fa-spin fa-3x fa-fw"></i>');

					// Iterate over each team
					$teams.each((_, element) => {

						let $element = $(element),
								id = $element[0].id,
								name = $element.find('.edit-field').text();

						// Add team to array
						teams.push({
							id: id,
							name: name,
							score: 0,
						});

					});

					// Save game options
					applicationData.game.settings.teams = teams;
					applicationData.game.settings.roundTime = roundTime;
					applicationData.game.settings.gameType = gameType;
					applicationData.game.settings.rounds = rounds;
					applicationData.game.settings.points = points;

					// Set cookie with game settings
					cookieHandler.setCookie(
						enumElements.cookies.gameSettings,
						applicationData.game.settings
					);

					// Start a new game
					this.game.newGame();

				// Else show an error message
				} else {

					// Set error message
					$infobox.text(enumText.input.selectGametype)
					.addClass('alert');

					// Animate message
					utility.refreshAnimation(
						$infobox[0],
						'slide-in'
					);

					// Focus gametype element
					$gameType.focus();

				}

			}
		);

	}

	// Animate clock change
	_clockToggleTime($element, time) {

		let unit = $element.data('unit'),
				prevTime = $element.data('time'),
				animate = false,
				animation = 'clock-fade-in',
				sumTime = time;

		// Is the time in minutes or seconds
		time = (unit == 'm' ? Math.floor(time / 60) : time % 60);

		// Check if we need to add left align class for minute element
		if(unit == 'm' && sumTime % 60 > 0) {

			$element.addClass('align-left');

		} else {

			$element.removeClass('align-left');

		}

		// Has the time changed from the previous time
		if(time !== prevTime) {

			// Set the new time
			$element.data('time', time);

			// Set to animate
			animate = true;

			// Is the time unit 0
			if(time === 0) {

				// Set the element to animate out
				animation = 'clock-fade-out';

			} else {

				// Otherwise set the element text to the new time
				$element.text(`${time}${unit}`);

			}

		}

		// Should we animate
		if(animate) {

			// Remove all animation classes
			$element.removeClass('hide clock-fade-in clock-fade-out');

			// Rerun animation
			utility.refreshAnimation(
				$element,
				animation
			);

		}

	}

}