'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';
import utility from '../utilities/utilities.js';

export default class PreTurnEvents {

	constructor(gameLoop) {

		// Game loop handler
		this.game = gameLoop;

		this._events();

	}

	// Event listeners
	_events() {

		let preTurn = enumElements.wrapper.preTurn;

		// On animation end for start turn button
		$(document).on(
			'animationend webkitAnimationEnd oAnimationEnd',
			`${preTurn.startTurnButton.id} .animation-wrapper`,
			() => {

				this.game.startTurn();

			}
		);

		// Mouse down event on start turn button
		$(document).on(
			'mousedown touchstart',
			preTurn.startTurnButton.id,
			function() {

				let $element = $(this),
						$animationElement = $element.find('.animation-wrapper');

				// Run animation
				utility.refreshAnimation(
					$animationElement[0],
					'fill'
				);

			}
		);

		// Mouse up event on start turn button
		$(document).on(
			'mouseup mouseout touchend',
			preTurn.startTurnButton.id,
			function() {

				let $element = $(this),
						$animationElement = $element.find('.animation-wrapper');

				// Remove animation class
				$animationElement.removeClass('fill');

			}
		);

	}

}