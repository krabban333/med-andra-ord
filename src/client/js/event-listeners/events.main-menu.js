'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';
import applicationData from '../utilities/utilities.application-data.js';
import cookieHandler from '../utilities/utilities.cookie-handler.js';
import utility from '../utilities/utilities.js';

export default class MenuEvents {

	constructor(contentHandler) {

		// Content handler instance
		this.content = contentHandler;

		this._events();

	}

	displayOptionsPage() {

		// Get game settings cookie
		let gameSettings = cookieHandler.getCookie(enumElements.cookies.gameSettings);

		// Does the game settings cookie exist
		if(gameSettings) {

			let gameOptions = enumElements.wrapper.gameOptions,
					$teamSlider = $(`${gameOptions.teamsSlider.id} input`),
					$teamEditor = $(gameOptions.teamEditor.id),
					$roundTime = $(gameOptions.roundClock.id),
					$gameType = $(gameOptions.gameType.id),
					$numRounds = $(gameOptions.numRounds.id),
					$numPoints = $(gameOptions.numPoints.id),
					settings;

			// Use settings saved in the cookie
			applicationData.game.settings = JSON.parse(gameSettings);

			// Assign settings to variable for easier access
			settings = applicationData.game.settings;

			// Set correct number of teams
			$teamSlider.val(settings.teams.length)
			.trigger('input');

			// Set teams
			settings.teams.forEach((team) => {

				$(`#${team.id}`).find('.edit-field')
				.text(team.name);

			});

			// Set correct round time
			this._setClock(
				$roundTime[0],
				settings.roundTime
			);
			this._disableInput(
				$roundTime[0],
				settings.roundTime
			);

			// Set correct gametype
			$gameType.find('select')
			.val(settings.gameType)
			.trigger('change');

			// Set correct rounds
			this._setCounter(
				$numRounds[0],
				settings.rounds
			);
			this._disableInput(
				$numRounds[0],
				settings.rounds
			);

			// Set correct points
			$numPoints.find('input')
			.val(settings.points)
			.trigger('change');

		}

		// Set thumb to correct position for each range element
		utility.setRangesliderThumb();

	}

	// Event listeners
	_events() {

		// On start game button click
		$(document).on(
			'click',
			enumElements.wrapper.menuMain.optNewGame.id,
			() => {

				// Render options page
				this.content.renderContent(
					enumElements.wrapper.id,
					'GameOptions',
					undefined,
					() => this.displayOptionsPage()
				);

				// Disable toolbar back button;
				utility.toggleToolbarBtn(
					enumElements.header.toolbar.optBack.id, 
					true
				);

			}
		);

		// On help button click
		$(document).on(
			'click',
			enumElements.wrapper.menuMain.optHelp.id,
			() => {

				// Render options page
				this.content.renderContent(
					enumElements.wrapper.id,
					'GameHelp'
				);

				// Disable toolbar back button;
				utility.toggleToolbarBtn(
					enumElements.header.toolbar.optBack.id, 
					true
				);

			}
		);

	}

	// Set default value for clock
	_setClock(clock, value) {

		let gameOptions = enumElements.wrapper.gameOptions,
				$clock = $(clock),
				timeObj = {
					minutes: $clock.find(gameOptions.roundClock.minutes.classes),
					seconds: $clock.find(gameOptions.roundClock.seconds.classes),
				},
				minutes = Math.floor(value / 60),
				seconds = value % 60;

		// Set correct value
		$clock.data('value', value);
		timeObj.minutes.data('time', minutes);
		timeObj.seconds.data('time', seconds);
		timeObj.minutes.text(`${minutes}m`);
		timeObj.seconds.text(`${seconds}s`);

		// Is minutes 0
		if(minutes === 0) {

			// Hide minutes element
			timeObj.minutes.addClass('hide');

		} else {

			// Display minutes element
			timeObj.minutes.removeClass('hide');

		}

		// Is seconds 0
		if(seconds === 0) {

			// Hide seconds element
			timeObj.seconds.addClass('hide');

		} else {

			// Display seconds element
			timeObj.seconds.removeClass('hide');

		}

	}

	// Set default value for counter
	_setCounter(counter, value) {

		let $counter = $(counter),
				$value = $counter.find('.value');

		// Set correct value
		$counter.data('value', value);
		$value.text(value);

	}

	// Check if input buttons should be disabled
	_disableInput(counter, value) {

		let $counter = $(counter),
				$plus = $counter.find('.controls .plus'),
				$minus = $counter.find('.controls .minus'),
				min = $counter.data('min'),
				max = $counter.data('max');

		// Disable plus button if max is reached
		if(value === max) {

			$plus.addClass('disabled');

		}

		// Disable minus button if min is reached
		if(value === min) {

			$minus.addClass('disabled');

		}

	}

}