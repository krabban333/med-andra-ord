'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';

export default class ScoreEvents {

	constructor(gameLoop) {

		// Game loop handler
		this.game = gameLoop;

		this._events();

	}

	// Event listeners
	_events() {

		let score = enumElements.wrapper.score;

		// On animation end for last list element
		$(document).on(
			'animationend webkitAnimationEnd oAnimationEnd',
			`${score.display.id} .team.last`,
			() => {

				let $scoreDisplay = $(score.display.id);

				// Restore wrapper height when animation has finished
				$scoreDisplay.css({height: 'auto'})

			}
		);

		// On next round button click
		$(document).on(
			'click',
			score.nextRound.id,
			() => this.game.newRound()
		);

		// On back button click
		$(document).on(
			'click',
			score.back.id,
			() => this.game.displayEndScreen()
		);

	}

}