'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';
import utility from '../utilities/utilities.js';

export default class TurnEvents {

	constructor(gameLoop, mediaHandler) {

		// Game loop handler
		this.game = gameLoop;

		// Media handler
		this.media = mediaHandler;

		this._events();

	}

	// Event listeners
	_events() {

		let turn = enumElements.wrapper.turn;

		// Animate control buttons on click
		$(document).on(
			'click',
			`${turn.word.id} ${turn.word.controls.classes} > span`,
			function() {

				let $element = $(this),
						$icon = $element.children('svg');

				// Refresh animation
				utility.refreshAnimation(
					$icon[0],
					'bounce'
				);

			}
		);

		// On correct word click
		$(document).on(
			'click',
			turn.word.controls.correct.id,
			() => {

				// Play audio file
				this.media.playAudio('correct');

				// Add a point and display a new word
				this.game.correctWord();

			}
		);

		// On skip word click
		$(document).on(
			'click',
			turn.word.controls.skip.id,
			() => {

				// Play audio file
				this.media.playAudio(
					'skip',
					0.4
				);

				// Display a new word
				this.game.skipWord();

			}
		);

	}

}