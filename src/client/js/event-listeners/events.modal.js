'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';

export default class ModalEvents {

	constructor(modal) {

		// Modal instance
		this.modal = modal;

		this._events();

	}

	// Event listeners
	_events() {

		// Need to define our modal class here because the 'this' variable is overwritten
		const modal = this.modal;

		// On swal2 button click
		$(document).on(
			'click',
			`${enumElements.modal.classes} .swal2-footer .btn`,
			// ES6 doesn't bind a 'this' variable, use a regular function
			function() {

				let $element = $(this),
						action = $element.data('action');

				// Run correct action
				modal.selectAction(action);

			}
		);

	}

}