'use strict';

// Import our modules and classes
import $ from 'jquery';
import Modal from '../modal/modal.js';
import enumElements from '../utilities/enum.elements.js';
import utility from '../utilities/utilities.js';

// Our event listeners
import DropdownEvents from './events.dropdown.js';
import MenuEvents from './events.main-menu.js';
import ModalEvents from './events.modal.js';
import HeaderEvents from './events.header.js';
import GameOptionsEvents from './events.game-options.js';
import PreTurnEvents from './events.pre-turn.js';
import TurnEvents from './events.turn.js';
import ScoreEvents from './events.score.js';
import EndEvents from './events.end.js';

// Need to define our modal class here because the 'this' variable is overwritten
const modal = new Modal();

export default class EventListeners {

	constructor(contentHandler, gameLoop, mediaHandler) {

		// Content handler instance
		this.content = contentHandler;

		// Game loop handler
		this.game = gameLoop;

		// Media handler
		this.media = mediaHandler;

		// Instances of our event listeners
		this.dropdownEvents = new DropdownEvents(contentHandler, modal);
		this.menuEvents = new MenuEvents(contentHandler);
		this.modalEvents = new ModalEvents(modal);
		this.headerEvents = new HeaderEvents(contentHandler, modal, gameLoop);
		this.gameOptionsEvents = new GameOptionsEvents(gameLoop, mediaHandler);
		this.PreTurnEvents = new PreTurnEvents(gameLoop);
		this.TurnEvents = new TurnEvents(gameLoop, mediaHandler);
		this.ScoreEvents = new ScoreEvents(gameLoop);
		this.EndEvents = new EndEvents(contentHandler, gameLoop, this.menuEvents);

		this._events();

	}

	// General event listeners
	_events() {

		let resizeTimer;

		// On resize event
		$(window).on(
			'resize',
			() => {

				let $turnTitle = $(enumElements.wrapper.turn.title.id),
						$separator = $turnTitle.find('.separator');

				clearTimeout(resizeTimer);

				// Debounce resize event
				resizeTimer = setTimeout(() => {

					// Set thumb to correct position for each range element
					utility.setRangesliderThumb();

					// Set wrapper height
					this.content.setHeight(
						enumElements.wrapper.id, 
						enumElements.header.id,
						enumElements.footer.id
					);

				}, 300);

			}
		);

		// On animation end for preloader
		$(document).on(
			'animationend webkitAnimationEnd oAnimationEnd',
			enumElements.preloader.id,
			() => {

				let $preloader = $(enumElements.preloader.id);

				// Remove preloader element when animation has finished
				$preloader.remove();

			}
		);

		// On keydown
		$(document).on(
			'keydown',
			(event) => {

				// Let modal class handle keydown if modal is visible
				if(modal.isVisible()) {

					modal.keydownHandler(event);

				}

			}
		);

		// Keyup on input element
		$(document).on(
			'keyup',
			'input, textarea',
			function(event) {

				let $element = $(this),
						value = $element.val();

				// Remove extra whitespaces
				$element.val(utility.trimWhitespaces(value));

			}
		);

		// Click on button element
		$(document).on(
			'click',
			'button',
			() => {

				// Play click sound
				this.media.playAudio(
					'click',
					0.2
				);

			}
		);

	}

}