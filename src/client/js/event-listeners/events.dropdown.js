'use strict';

// Import our modules and classes
import $ from 'jquery';
import session from '../session/session.js';
import enumElements from '../utilities/enum.elements.js';

export default class DropdownEvents {

	constructor(contentHandler, modal) {

		// Content handler instance
		this.content = contentHandler;

		// Modal instance
		this.modal = modal;

		this._events();

	}

	// Event listeners
	_events() {

		// On login option click
		$(document).on(
			'click',
			enumElements.footer.dropdownMenu.optLogin.id,
			() => {

				// Don't create the login modal if it's already being displayed
				if(!this.modal.isVisible()) {

					// Create modal
					this.modal.createModal('modalLogin', () => {

						// Update session on login submit
						session.get()
						.then(() => {

							this.content.renderFooter();

							// Show success modal
							this.modal.createModal('modalSuccess');

						});

					});

				}

			}
		);

		// On logout option click
		$(document).on(
			'click',
			enumElements.footer.dropdownMenu.optLogout.id,
			() => {

				this.modal.createModal('modalLogout', () => {

					// Destroy current user session 
					session.destroy();

					this.content.renderFooter();

				})

			}
		);

	}

}