'use strict';

// Import our modules and classes
import $ from 'jquery';
import applicationData from '../utilities/utilities.application-data.js';
import enumElements from '../utilities/enum.elements.js';
import utility from '../utilities/utilities.js';

export default class HeaderEvents {

	constructor(contentHandler, modal, gameLoop) {

		// Content handler instance
		this.content = contentHandler;

		// Modal instance
		this.modal = modal;

		// Game loop handler
		this.game = gameLoop;

		this._events();

	}

	// Event listeners
	_events() {

		// On back button click
		$(document).on(
			'click',
			enumElements.header.toolbar.optBack.id,
			() => {

				if(applicationData.gameStarted) {

					// Don't create the modal if it's already being displayed
					if(!this.modal.isVisible()) {

						// Create modal
						this.modal.createModal('modalConfirmEndgame', () => {

							applicationData.gameStarted = false;

							// Clear all game related variables
							this.game.resetGame();

							this._displayMainMenu();

						});

					}

				} else {

					this._displayMainMenu();

				}

			}
		);

		// On mute button click
		$(document).on(
			'click',
			enumElements.header.toolbar.optMute.id,
			function() {

				let $element = $(this);

				if(applicationData.mute) {

					// Enable sound
					applicationData.mute = false;

					// Set correct icon
					$element.html('<i class="fas fa-volume-up"></i>');

				} else {

					// Mute sound
					applicationData.mute = true;

					// Set correct icon
					$element.html('<i class="fas fa-volume-mute"></i>');

				}

			}
		);

		// On fullscreen button click
		$(document).on(
			'click',
			enumElements.header.toolbar.optFullscreen.id,
			function() {

				let $element = $(this);

				// Check if window is in fullscreen
				if((window.fullScreen) || (window.innerWidth == screen.width && window.innerHeight == screen.height)) {

					// Try to exit fullscreen with the default function
					if (document.exitFullscreen) {
						document.exitFullscreen();

					// Try to exit fullscreen with the firefox function
					} else if (document.mozCancelFullScreen) {
						document.mozCancelFullScreen();

					// Try to exit fullscreen with the chromium function
					} else if (document.webkitExitFullscreen) {
						document.webkitExitFullscreen();

					// Try to exit fullscreen with the explorer function
					} else if (document.msExitFullscreen) {
						document.msExitFullscreen();

					}

				// View window in fullscreen if it's not already fullscreen
				} else {

					let elem = document.documentElement;

					// Try to run the default fullscreen request
					if (elem.requestFullscreen) {
						elem.requestFullscreen();

					// Else try to run the firefox fullscreen request
					} else if (elem.mozRequestFullScreen) {
						elem.mozRequestFullScreen();

					// Else try to run the chromium fullscreen request
					} else if (elem.webkitRequestFullscreen) {
						elem.webkitRequestFullscreen();

					// Else try to run the explorer fullscreen request
					} else if (elem.msRequestFullscreen) {
						elem.msRequestFullscreen();

					}

				}

			}
		);

		// Add eventlistener for the fullscreen event
		// This is done to set the correct fullscreen icon even if the user exits fullscreen in other ways then pressing the toolbar button
		['', 'webkit', 'moz', 'ms'].forEach(
			prefix => document.addEventListener(
				prefix + "fullscreenchange", 
				() => {

					let $element = $(enumElements.header.toolbar.optFullscreen.id);

					// Are we already in fullscreen mode?
					if((window.fullScreen) || (window.innerWidth == screen.width && window.innerHeight == screen.height)) {

						// Set correct icon
						$element.html('<i class="fas fa-compress"></i>');

					} else {

						// Set correct icon
						$element.html('<i class="fas fa-expand"></i>');

					}

				}
			), 
			false
		);

	}

	// Display the main menu
	_displayMainMenu() {

		// Render header
		this.content.renderContent(
			enumElements.header.id,
			'Header'
		);

		// Render main menu
		this.content.renderContent(
			enumElements.wrapper.id,
			'MainMenu'
		);

		// Disable toolbar back button;
		utility.toggleToolbarBtn(
			enumElements.header.toolbar.optBack.id, 
			false
		);

	}

}