'use strict';

// Import our modules and classes
import React from 'react';
import ReactDOM from 'react-dom';
import M from 'materialize-css';
import session from '../session/session.js';
import enumElements from '../utilities/enum.elements.js';
import utility from '../utilities/utilities.js';

// React components
import Header from '../react-components/header/header';
import Toolbar from '../react-components/toolbar/toolbar';
import Footer from '../react-components/footer/footer';
import MainMenu from '../react-components/main-menu/main-menu';
import GameOptions from '../react-components/game-options/game-options';
import GameHelp from '../react-components/game-help/game-help';
import PreTurn from '../react-components/pre-turn/pre-turn';
import Turn from '../react-components/turn/turn';
import Score from '../react-components/score/score';
import End from '../react-components/end/end';

export default class ContentHandler {

	constructor(mediaHandler) {

		// Media handler
		this.media = mediaHandler;

		// Available components
		this.components = {
			Header: Header,
			Toolbar: Toolbar,
			Footer: Footer,
			MainMenu: MainMenu,
			GameOptions: GameOptions,
			GameHelp: GameHelp,
			PreTurn: PreTurn,
			Turn: Turn,
			Score: Score,
			End: End,
		}

		// Dropdown menu element
		this.dropdownElement = '';

		// Display the page
		this._initPage();

	}

	// Renders the header
	renderHeader() {

		this.renderContent(
			enumElements.header.id,
			'Header'
		);

	}

	// Renders the footer
	renderFooter() {

		this.renderContent(
			enumElements.footer.id,
			'Footer',
			undefined,
			() => {

				// Initialize dropdown
				this._initDropdown(enumElements.footer.dropdownMenu.classes);

			}
		);

	}

	// Render page content
	renderContent(target, type, properties = undefined, callback = undefined) {

		let DynamicComponent = this.components[type];

		ReactDOM.render(
			<DynamicComponent properties={properties} />,
			document.querySelectorAll(target)[0],
			() => {

				// Initialize materialize components
				M.AutoInit();

				// Recalculate wrapper height
				this.setHeight(
					enumElements.wrapper.id, 
					enumElements.header.id,
					enumElements.footer.id
				);

				// Resize dynamic text
				utility.fitText();

				// Set scroll to top of page
				window.scrollTo(0, 0);

				// Run callback if set
				(typeof callback === 'function' && callback());

			}
		);

	}

	// Calculates height of element1 - element2
	setHeight(wrapper, header, footer) {

		let setElement = document.querySelector(wrapper),
				heightElement1 = document.querySelector(header),
				heightElement2 = document.querySelector(footer);

		setElement.style.minHeight = `calc(100vh - ${heightElement1.clientHeight + heightElement2.clientHeight}px)`;

	}

	// Initialize page
	_initPage() {

		// Get session
		session.get()
		.then(() => {

			this.renderHeader();

			// Render main menu
			this.renderContent(
				enumElements.wrapper.id,
				'MainMenu',
				undefined,
				() => {

					let preloader = document.querySelector(enumElements.preloader.id),
							audioInterval;

					// Check if audio is ready on an interval
					// There are better ways to do this but ehhh
					audioInterval = setInterval(() => {

						// Is the audio ready
						if(this.media._audioReady()) {

							clearInterval(audioInterval);

							// Fade out preloader
							preloader.classList.add('fade-out-slow');

						}

					}, 300);

				}
			);

			this.renderFooter();

		})
		.catch((err) => console.error('Could not render page:', err));

	}

	// Initialize dropdown menu
	_initDropdown(target, options = undefined) {

		let element = document.querySelectorAll(`${target} .dropdown-trigger`),
				listElements = document.querySelectorAll(`${target} .dropdown-content li`);
		
		// Check if options are set, otherwise use default options
		options = (options !== undefined ? options : {
			alignment: 'right',
			autoTrigger: false,
			closeOnClick: false,
			constraintWidth: false,
			coverTrigger: false,
			inDuration: 200,
			outDuration: 300,
		});

		// Initialize dropdown menu
		this.dropdownElement = M.Dropdown.init(
			element,
			options
		);

		// Bind close event on all list options except those with class disabled
		for(let i = 0; i < listElements.length; i++) {

			let event = () => this.dropdownElement[0].close();

			if(listElements[i].classList.contains('disabled')) {

				event = () => {return false};

			}

			listElements[i].onclick = event;

		}

	}

}