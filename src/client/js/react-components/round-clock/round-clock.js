'use strict';

// Import our modules and classes
import React from 'react';
import enumElements from '../../utilities/enum.elements.js';

export default class RoundClock extends React.Component {

	render() {

		// Component properties
		const {
			id,
			minutes,
			seconds,
			max,
			min,
			step,
		} = this.props;

		let roundClock = enumElements.wrapper.gameOptions.roundClock,
				minutesClass = enumElements.noPrefix(roundClock.minutes.classes),
				secondsClass = enumElements.noPrefix(roundClock.seconds.classes),
				value = (minutes * 60) + seconds;

		minutesClass += (minutes === 0 ? ' hide' : '');
		secondsClass += (seconds === 0 ? ' hide' : '');

		return (
			<div id={id} className="counter" data-value={value} data-max={max} data-min={min} data-step={step}>
				<div className="inner-wrapper">
					<div className="value">
						<span className={minutesClass} data-time={minutes} data-unit="m">{minutes}m</span>
						<span className={secondsClass} data-time={seconds} data-unit="s">{seconds}s</span>
					</div>
					<span className="fa-stack">
						<i className="fas fa-circle fa-stack-2x"></i>
						<i className="far fa-clock fa-stack-1x"></i>
					</span>
					<div className="controls">
						<span className="minus" data-action="minus">
							<i className="far fa-minus-square"></i>
						</span>
						<span className="plus" data-action="plus">
							<i className="far fa-plus-square"></i>
						</span>
					</div>
				</div>
			</div>
		); 

	}

}