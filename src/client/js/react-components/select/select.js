'use strict';

// Import our modules and classes
import React from 'react';

export default class Select extends React.Component {

	render() {

		// Component properties
		const {
			id, 
			label, 
			content, 
			defaultValue,
		} = this.props;

		return (
			<div className="select-wrapper">

				<div id={id} className="input-field">
					<select defaultValue={defaultValue}>

						{content.map((option, index) => {

							return (
								<option 
									key={index} 
									id={option.id} 
									value={option.value} 
									data-description={option.description} 
									data-tabs={option.displayTabs}
									disabled={!option.active}
								>
									{option.text}
								</option>);

						})}

					</select>
					{(label) && ([
						<label key="0">{label}</label>
					])}
				</div>

				{this.props.children}

			</div>
		);

	}

}

// Default values
Select.defaultProps = {
	defaultValue: 0,
}