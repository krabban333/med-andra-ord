'use strict';

// Import our modules and classes
import applicationData from '../../utilities/utilities.application-data.js';
import enumElements from '../../utilities/enum.elements.js';

let toolbar = enumElements.header.toolbar;

// Our dropdown options
const enumToolbarOptions = [
	{
		id: enumElements.noPrefix(toolbar.optBack.id),
		type: 'btn',
		icon: () => {return 'fa-long-arrow-alt-left';},
		active: false,
	},
	{
		id: enumElements.noPrefix(toolbar.optMute.id),
		type: 'flat',
		icon: () => {return (applicationData.mute ? 'fa-volume-mute' : 'fa-volume-up');},
		active: true,
	},
	{
		id: enumElements.noPrefix(toolbar.optFullscreen.id),
		type: 'flat',
		icon: () => {return (applicationData.fullscreen ? 'fa-compress' : 'fa-expand');},
		active: true,
	},
];

export default enumToolbarOptions;