'use strict';

// Import our modules and classes
import React from 'react';
import enumElements from '../../utilities/enum.elements.js';
import enumToolbarOptions from './enum.toolbar-options.js';

// React components
import Button from '../button/button';

export default class Toolbar extends React.Component {

	render() {

		let id = enumElements.noPrefix(enumElements.header.toolbar.id);

		return (
			<div id={id}>

				{/* Loop through and output all toolbar options */}
				{enumToolbarOptions.map((option, index) => {

					return <Button key={index} id={option.id} type={option.type} icon={option.icon()} active={option.active} />;

				})}

			</div>
		);

	}

}