'use strict';

// Import our modules and classes
import React from 'react';
import enumGametypeOptions from './enum.gametype-options.js';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';

// React components
import PageTitle from '../page-title/page-title';
import Card from '../card/card';
import RangeSlider from '../range-slider/range-slider';
import TeamEditor from '../team-editor/team-editor';
import RoundClock from '../round-clock/round-clock';
import Select from '../select/select';
import Infobox from '../infobox/infobox';
import Counter from '../counter/counter';
import Button from '../button/button';

export default class GameOptions extends React.Component {

	render() {

		let gameOptions = enumElements.wrapper.gameOptions,
				text = enumText.gameOptions,
				wrapper = {
					id: enumElements.noPrefix(gameOptions.id),
				},
				teamSlider = {
					id: enumElements.noPrefix(gameOptions.teamsSlider.id),
					extraClasses: 'display-thumb',
					min: 2,
					max: 10,
				},
				teamEditor = {
					id: enumElements.noPrefix(gameOptions.teamEditor.id),
				},
				roundClock ={
					id: enumElements.noPrefix(gameOptions.roundClock.id),
					minutes: 0,
					seconds: 30,
					min: 10,
					max: 120,
					step: 10,
				},
				gameType = {
					id: enumElements.noPrefix(gameOptions.gameType.id),
					helpIconId: enumElements.noPrefix(gameOptions.gameType.help.icon.id),
					helpInfoId: enumElements.noPrefix(gameOptions.gameType.help.infoBox.id),
				},
				roundsCounter = {
					id: enumElements.noPrefix(gameOptions.numRounds.id),
					min: 1,
					max: 30,
					value: 3,
					icon: 'fas fa-crown',
				},
				pointsCounter = {
					id: enumElements.noPrefix(gameOptions.numPoints.id),
					extraClasses: 'display-thumb',
					min: 1,
					max: 100,
					value: 10,
				},
				startGameButton = {
					id: enumElements.noPrefix(gameOptions.startGame.id),
				};

		return (
			<div id={wrapper.id}>

				{/* Page title */}
				<PageTitle title={text.title} />

				{/* Team card */}
				<Card title={text.teamSlider.title}>

					{/* Number of teams slider */}
					<RangeSlider 
						id={teamSlider.id} 
						extraClasses={teamSlider.extraClasses} 
						min={teamSlider.min} 
						max={teamSlider.max} 
						defaultValue={teamSlider.min} 
					/>
					
					{/* Team name editor */}
					<TeamEditor 
						id={teamEditor.id} 
						title={text.teamEditor.title} 
						items={teamSlider.min} 
					/>

				</Card>

				{/* Round time card */}
				<Card title={text.roundTime.title}>

					{/* Roundtime representation */}
					<RoundClock 
						id={roundClock.id} 
						minutes={roundClock.minutes} 
						seconds={roundClock.seconds} 
						max={roundClock.max} 
						min={roundClock.min} 
						step={roundClock.step} 
					/>

				</Card>

				{/* Game type card */}
				<Card title={text.gameType.title}>

					{/* Game type selection */}
					<Select 
						id={gameType.id} 
						content={enumGametypeOptions}
					>
						<Infobox 
							iconId={gameType.helpIconId} 
							infoboxId={gameType.helpInfoId} 
						/>
					</Select>

					{/* Rounds */}
					<div className="tab tab-1">
						<Counter 
							id={roundsCounter.id} 
							title={text.numRounds.title}
							min={roundsCounter.min} 
							max={roundsCounter.max} 
							value={roundsCounter.value} 
							icon={roundsCounter.icon}
						/>
					</div>

					{/* Points */}
					<div className="tab tab-2">
						<RangeSlider 
							id={pointsCounter.id}
							title={text.numPoints.title}
							extraClasses={teamSlider.extraClasses} 
							min={pointsCounter.min} 
							max={pointsCounter.max} 
							defaultValue={pointsCounter.value} 
						/>
					</div>

				</Card>

				{/* Start game */}
				<Button
					id={startGameButton.id} 
					text={text.startGame.title}
					type="large"
				/>

			</div>
		);

	}

}