'use strict';

// Import our modules and classes
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';

let gametype = enumElements.wrapper.gameOptions.gameType,
		text = enumText.gameOptions.gameType;

// Our dropdown options
const enumGametypeOptions = [
	{
		id: 'default',
		value: 0,
		text: text.optDefault.text,
		active: false,
	},
	{
		id: enumElements.noPrefix(gametype.optRounds.id),
		value: 1,
		text: text.optRounds.text,
		description: text.optRounds.description,
		displayTabs: [
			'tab-1',
		],
		active: true,
	},
	{
		id: enumElements.noPrefix(gametype.optPoints.id),
		value: 2,
		text: text.optPoints.text,
		description: text.optPoints.description,
		displayTabs: [
			'tab-2',
		],
		active: false,
	},
	{
		id: enumElements.noPrefix(gametype.optMixed.id),
		value: 3,
		text: text.optMixed.text,
		description: text.optMixed.description,
		displayTabs: [
			'tab-1',
			'tab-2',
		],
		active: false,
	},
];

export default enumGametypeOptions;