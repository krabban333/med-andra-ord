'use strict';

// Import our modules and classes
import React from 'react';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';

// React components
import PageTitle from '../page-title/page-title';

export default class PreTurn extends React.Component {

	render() {

		// Component properties
		const {
			properties,
		} = this.props;

		let preTurn = enumElements.wrapper.preTurn,
				wrapper = {
					id: enumElements.noPrefix(preTurn.id),
				},
				teamName = {
					classes: enumElements.noPrefix(preTurn.teamName.classes),
				},
				startTurnButton = {
					id: enumElements.noPrefix(preTurn.startTurnButton.id),
				};

		return (
			<div id={wrapper.id}>

				{/* Page title */}
				<PageTitle title={enumText.preTurn.title} />

				{/* Team */}
				<h2 className={teamName.classes}>{properties.teamName}</h2>

				{/* Start turn button */}
				<div id={startTurnButton.id}>
					<i className='fas fa-play icon'></i>
					<svg className="animation-wrapper" viewBox="0 -1 200 200">
						<circle cx="100" cy="100" r="100"/>
					</svg>
				</div>

			</div>
		);

	}

}