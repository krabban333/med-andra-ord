'use strict';

// Import our modules and classes
import React from 'react';
import applicationData from '../../utilities/utilities.application-data.js';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';
import utility from '../../utilities/utilities.js';

// React components
import Button from '../button/button';

export default class End extends React.Component {

	componentDidMount() {

		let endWrapper = document.querySelector(enumElements.wrapper.end.id),
				podiums = document.querySelectorAll('.podium'),
				delay = 300;

		// Loop over each podium
		for(let i = 0; i < podiums.length; i++) {

			// Get number of children
			let numOfChildren = podiums[i].childNodes.length;

			// One child
			if(numOfChildren === 1) {

				// Use large font and icon
				podiums[i].classList.add('icon-lg');

			// Two children
			} else if(numOfChildren === 2) {

				// Use medium font and icons
				podiums[i].classList.add('icon-md');

			// More than two children
			} else {

				// Use small font and icons
				podiums[i].classList.add('icon-sm');

			}

			// Add class to last element
			if(i === podiums.length - 1) {

				podiums[i].classList.add('last');

			}

		}

		// Push to bottom of queue
		setTimeout(_ => {

			// Set wrapper height before we start our animations
			endWrapper.style.height = `${endWrapper.clientHeight}px`;

			// Make wrapper and content visible
			endWrapper.classList.add('animate');

			// Loop over each podium
			for(let i = 0; i < podiums.length; i++) {

				// Add delay to animations
				setTimeout(_ => {

					// Animate
					podiums[i].classList.add('scale-up');

				}, delay * i);

			}

		}, 0);

	}

	render() {

		let end = enumElements.wrapper.end,
				wrapper = {
					id: enumElements.noPrefix(end.id),
				},
				scoreButton = {
					id: enumElements.noPrefix(end.buttons.score.id),
					text: enumText.end.buttons.score,
				},
				newGameButton = {
					id: enumElements.noPrefix(end.buttons.newGame.id),
					text: enumText.end.buttons.newGame,
				},
				mainMenuButton = {
					id: enumElements.noPrefix(end.buttons.mainMenu.id),
					text: enumText.end.buttons.mainMenu,
				},
				teamArray = applicationData.game.settings.teams.slice();

		const generateHTML = (filter) => {

			let html = [];

			// Loop over all teams
			for(let index = 0; index <= teamArray.length - 1; index++) {

				let team = teamArray[index];

				// Does the team fit the filter
				if(team.icon == filter && team.score !== 0) {

					// Add html to array
					html.push(
						<div key={index} id={team.id} className={`team ${filter}`}>
							<span className="score-wrapper">
								<span className="score">{team.score}</span>
								<i className={`fas fa-trophy icon ${filter}`}></i>
							</span>
							<span className="name">
								<span className="inner">{team.name}</span>
							</span>
						</div>
					);

				}

			}

			return html;

		}

		// Sort array on score
		utility.sortScore(teamArray);

		// Assign gold, silver and bronze
		utility.setPlacings(teamArray);

		return (
			<div id={wrapper.id}>

				<div className="first-place podium">
					{generateHTML('gold')}
				</div>

				<div className="second-place podium">
					{generateHTML('silver')}
				</div>

				<div className="third-place podium">
					{generateHTML('bronze')}
				</div>

				<div className="button-wrapper">
					<Button id={scoreButton.id} type="large" text={scoreButton.text} />
					<Button id={mainMenuButton.id} type="large" text={mainMenuButton.text} />
					<Button id={newGameButton.id} type="large" text={newGameButton.text} />
				</div>

			</div>
		);

	}

}