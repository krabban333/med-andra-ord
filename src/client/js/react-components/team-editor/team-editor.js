'use strict';

// Import our modules and classes
import React from 'react';
import enumText from '../../utilities/enum.text.js';

export default class TeamEditor extends React.Component {

	render() {

		// Component properties
		const {
			id,
			title,
			items = parseInt(items),
		} = this.props;

		// Array to hold team list
		let itemsArray = [];

		// Add default teams to array
		for(let index = 1; index <= parseInt(items); index++) {

			let id = `team-${index}`;

			itemsArray.push(
				<li key={index} id={id} className="collection-item">
					<i className="fas fa-pen"></i>
					<span className="edit-field" contentEditable="true" suppressContentEditableWarning="true">
						{enumText.gameOptions.teamEditor.team} {index}
					</span>
				</li>
			);

		}

		return (
			<ul id={id} className="collapsible">
				<li>
					<div className="collapsible-header">
						<span className="title">
							<i className="fas fa-chevron-right"></i>
							{title}
						</span>
						<span className="num-of-teams new badge" data-badge-caption="">
							<span>{items}</span>
							<i className="fas fa-users"></i>
						</span>
					</div>
					<div className="collapsible-body">
						<ul className="team-list collection">
							{itemsArray}
						</ul>
					</div>
				</li>
			</ul>
		); 

	}

}