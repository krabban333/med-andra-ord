'use strict';

// Import our modules and classes
import React from 'react';
import enumText from '../../utilities/enum.text.js';

// React components
import Toolbar from '../toolbar/toolbar';

export default class Header extends React.Component {

	render() {

		return (
			<div className="header-inner">
				<Toolbar />
			</div>
		);

	}

}