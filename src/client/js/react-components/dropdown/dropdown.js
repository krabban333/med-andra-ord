'use strict';

// Import our modules and classes
import React from 'react';

export default class Dropdown extends React.Component {

	render() {

		// Component properties
		const {
			id,
			icon,
			content
		} = this.props;

		// Create content id
		let contentID = `${id}-content`;

		return (
			<div id={id} className="dropdown-menu">

				{/* Dropdown trigger */}
				<a className="dropdown-trigger no-autoinit" data-target={contentID}>
					<i className={'fas ' + icon}></i>
				</a>

				{/* Dropdown content */}
				<ul id={contentID} className="dropdown-content">

					{content.map((option, index) => {

						let disabled = (option.active ? '' : 'disabled');

						return (
							option.display() 
							?
							<li key={index} id={option.id} className={disabled}>
								<a className="valign-wrapper">
									{option.text}
									{(typeof option.icon === 'string') && ([
										<i key={index} className={'fas ' + option.icon}></i>
									])}
								</a>
							</li>
							:
							''
						);

					})}

				</ul>

			</div>
		);

	}

}

// Default values
Dropdown.defaultProps = {
	icon: 'fa-bars'
}