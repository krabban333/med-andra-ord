'use strict';

// Import our modules and classes
import React from 'react';
import enumMenuOptions from './enum.menu-options.js';
import enumElements from '../../utilities/enum.elements.js';

// React components
import Button from '../button/button';

export default class MainMenu extends React.Component {

	render() {

		return (
			<div id={enumElements.noPrefix(enumElements.wrapper.menuMain.id)}>
				
				{/* Loop through and output all of our menu options */}
				{enumMenuOptions.map((option, index) => {

					return <Button key={index} id={option.id} type="large" text={option.text} active={option.active} />;

				})}

			</div>
		);

	}

}