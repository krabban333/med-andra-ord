'use strict';

// Import our modules and classes
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';

let menu = enumElements.wrapper.menuMain;

// Our menu options
const enumMenuOptions = [
	{
		id: enumElements.noPrefix(menu.optNewGame.id),
		text: enumText.mainMenu.optNewGame,
		active: true,
	},
	{
		id: enumElements.noPrefix(menu.optHelp.id),
		text: enumText.mainMenu.optHelp,
		active: true,
	},
	{
		id: enumElements.noPrefix(menu.optSuggestWord.id),
		text: enumText.mainMenu.optSuggestWord,
		active: false,
	},
];

export default enumMenuOptions;