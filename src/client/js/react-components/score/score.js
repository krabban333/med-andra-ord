'use strict';

// Import our modules and classes
import React from 'react';
import applicationData from '../../utilities/utilities.application-data.js';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';
import utility from '../../utilities/utilities.js';

// React components
import PageTitle from '../page-title/page-title';
import Button from '../button/button';

export default class Score extends React.Component {

	componentDidMount() {

		let scoreWrapper = document.querySelector(enumElements.wrapper.score.display.id),
				teamList = document.querySelectorAll('.team'),
				animation = 'slide-in-score',
				delay = 100;

		// Set wrapper height before we start our animations
		scoreWrapper.style.height = `${scoreWrapper.clientHeight - (6 * teamList.length)}px`;

		// Make wrapper and content visible
		scoreWrapper.style.visibility = 'visible';

		for(let index = 0; index < teamList.length; index++) {

			// Set max-height to zero
			teamList[index].style.maxHeight = 0;
			teamList[index].style.maxWidth = 0;

			// Is this the first element
			if(index === 0) {

				// Do animation
				teamList[index].classList.add(animation);

			} else {

				// Do animation with a delay
				setTimeout(() => {

					teamList[index].classList.add(animation);

				}, delay * index);

			}

		}

	}

	render() {

		// Component properties
		const {
			properties,
		} = this.props;

		let score = enumElements.wrapper.score,
				wrapper = {
					id: enumElements.noPrefix(score.id),
					title: `${enumText.turn.round} ${properties.round}`,
				},
				scoreDisplay = {
					id: enumElements.noPrefix(score.display.id),
				},
				nextRoundButton = {
					id: enumElements.noPrefix(score.nextRound.id),
					text: enumText.score.nextRound,
				},
				backButton = {
					id: enumElements.noPrefix(score.back.id),
					text: enumText.score.back,
				},
				teamArray = applicationData.game.settings.teams.slice(),
				teamsHTML = [],
				extraClasses = '',
				buttonHtml;

		// Sort array on score
		utility.sortScore(teamArray);

		// Assign gold, silver and bronze
		utility.setPlacings(teamArray);

		// Loop through each team
		for(let index = 0; index <= teamArray.length - 1; index++) {

			let team = teamArray[index],
					iconClasses;

			// Is this the last list element
			if(index === teamArray.length - 1) {

				// Add extra class
				extraClasses = 'last';

			}

			// Set icon classes
			iconClasses = `fas fa-trophy icon ${team.icon}`;

			// Push team HTML to array
			teamsHTML.push(
				<li id={team.id} className={`team ${extraClasses}`} key={index}>
					<span className="name">
						<span className="inner">{team.name}</span>
					</span>
					<span className="score-wrapper">
						<span className="score">{team.score}</span>
						<i className={iconClasses}></i>
					</span>
				</li>
			);

		}

		// Show back button if game has ended, else show next round button
		if(properties.gameOver) {

			buttonHtml = <Button id={backButton.id} type="large" text={backButton.text} />;

		} else {

			buttonHtml = <Button id={nextRoundButton.id} type="large" text={nextRoundButton.text} />;

		}

		return (
			<div id={wrapper.id}>

				{/* Page title */}
				<PageTitle title={wrapper.title} />

				{/* Score display */}
				<ul id={scoreDisplay.id}>
					{teamsHTML}
				</ul>

				{/* Next round button */}
				{buttonHtml}

			</div>
		);

	}

}