'use strict';

// Import our modules and classes
import React from 'react';
import Dropdown from '../dropdown/dropdown.js';
import enumDropdownOptions from './enum.dropdown-options.js';

export default class Footer extends React.Component {

	render() {

		return (
			<div className="footer-inner">

				<span className="version">Version {VERSION}</span>

				<div id="admin-menu-wrapper">
					<Dropdown key="0" id="admin-dropdown" icon="fa-cog" content={enumDropdownOptions} />
				</div>

			</div>
		);

	}

}