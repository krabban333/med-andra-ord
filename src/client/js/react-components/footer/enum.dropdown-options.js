'use strict';

// Import our modules and classes
import applicationData from '../../utilities/utilities.application-data.js';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';
import utility from '../../utilities/utilities.js';

let dropdown = enumElements.footer.dropdownMenu;

// Our dropdown options
const enumDropdownOptions = [
	{
		id: enumElements.noPrefix(dropdown.optUserSettings.id),
		text: enumText.dropdownMenu.optUserSettings,
		active: false,
		display: () => {

			return (
				!utility.isObjectEmpty(applicationData.user.capabilities) 
				&& 
				applicationData.user.capabilities.changeUserSettings
			);

		}
	},
	{
		id: enumElements.noPrefix(dropdown.optEditWords.id),
		text: enumText.dropdownMenu.optEditWords,
		active: false,
		display: () => {

			return (
				!utility.isObjectEmpty(applicationData.user.capabilities) 
				&& 
				(
					applicationData.user.capabilities.editWord 
					|| 
					applicationData.user.capabilities.approveWord 
					|| 
					applicationData.user.capabilities.removeWord
				)
			);

		}
	},
	{
		id: enumElements.noPrefix(dropdown.optFeedback.id),
		text: enumText.dropdownMenu.optFeedback,
		icon: 'fa-comment-alt',
		active: false,
		display: () => {

			return !applicationData.user.authenticated;

		},
	},
	{
		id: enumElements.noPrefix(dropdown.optLogin.id),
		text: enumText.dropdownMenu.optLogin,
		icon: 'fa-sign-in-alt',
		active: true,
		display: () => {

			return !applicationData.user.authenticated;

		},
	},
	{
		id: enumElements.noPrefix(dropdown.optLogout.id),
		text: enumText.dropdownMenu.optLogout,
		icon: 'fa-sign-out-alt',
		active: true,
		display: () => {

			return applicationData.user.authenticated;

		}
	},
];

export default enumDropdownOptions;