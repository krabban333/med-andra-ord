'use strict';

// Import our modules and classes
import React from 'react';

export default class Counter extends React.Component {

	render() {

		// Component properties
		const {
			title,
			id,
			min,
			max,
			step,
			value,
			icon,
			iconSecondary,
			iconTeriary,
		} = this.props;

		let plusExtraClasses = (value === max ? ' disabled' : ''),
				minusExtraClasses = (value === min ? ' disabled' : '');

		return (

			<div id={id} className="counter" data-value={value} data-max={max} data-min={min} data-step={step}>
				{(title) && ([
					<h2 key="0">{title}</h2>
				])}
				<div className="inner-wrapper">
					<div className="value">
						<span>{value}</span>
					</div>
					<span className="fa-stack">
						<i className={iconTeriary + ' fa-stack-2x tertiary'}></i>
						<i className={iconSecondary + ' fa-stack-2x'}></i>
						<i className={icon + ' fa-stack-1x'}></i>
					</span>
					<div className="controls">
						<span className={'minus' + minusExtraClasses} data-action="minus">
							<i className="far fa-minus-square"></i>
						</span>
						<span className={'plus' + plusExtraClasses} data-action="plus">
							<i className="far fa-plus-square"></i>
						</span>
					</div>
				</div>
			</div>
		);

	}

}

// Default values
Counter.defaultProps = {
	min: 0,
	max: 10,
	step: 1,
	value: 0,
	iconSecondary: 'fas fa-circle',
	iconTeriary: 'far fa-circle',
}