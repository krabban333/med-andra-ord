'use strict';

// Import our modules and classes
import React from 'react';

export default class Infobox extends React.Component {

	render() {

		// Component properties
		const {
			iconId,
			infoboxId,
			icon,
			iconSecondary,
		} = this.props;

		return ([
			<span key="0" id={iconId}>
				<i key="1" className={'open ' + icon}></i>
				{(iconSecondary) && ([
					<i key="2" className={'close ' + iconSecondary}></i>
				])}
			</span>,
			<div key="3" id={infoboxId} className="infobox"></div>
		]);

	}

}

// Default values
Infobox.defaultProps = {
	icon: 'far fa-question-circle',
	iconSecondary: 'far fa-times-circle',
}