'use strict';

// Import our modules and classes
import React from 'react';

export default class Button extends React.Component {

	render() {

		// Component properties
		const {
			id, 
			type, 
			waves, 
			text, 
			icon,
			active
		} = this.props;

		// Get classes
		let classes = this._getClass(type);

		// Should the button have the waves effect
		classes += (waves ? ' waves-effect waves-light' : '');

		// Is the button disabled
		classes += (active ? '' : ' disabled');

		return (
			<button className={'valign-wrapper ' + classes} id={id}>

				{/* Button text */}
				{text}

				{/* Should an icon be displayed */}
				{(typeof icon === 'string') && ([
					<i key="0" className={'fas ' + icon}></i>
				])}

			</button>
		);

	}

	// Get correct button class
	_getClass(type) {

		switch(type) {

			case 'large':
			case 'btn-large':
				return 'btn-large';

			case 'small':
			case 'btn-small':
				return 'btn-small';

			case 'floating':
			case 'btn-floating':
				return 'btn-floating';

			case 'flat':
			case 'btn-flat':
				return 'btn-flat';

			case 'btn':
			default:
				return 'btn';

		}

	}

}

// Default values
Button.defaultProps = {
	type: 'btn',
	waves: true,
	text: '',
	active: true,
}