'use strict';

// Import our modules and classes
import React from 'react';

export default class PageTitle extends React.Component {

	render() {

		// Component properties
		const {
			title,
		} = this.props;

		return (
			<div className="page-title">
				<h1>{title}</h1>
			</div>
		);

	}

}