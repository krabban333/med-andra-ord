'use strict';

// Import our modules and classes
import React from 'react';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';

export default class Turn extends React.Component {

	render() {

		// Component properties
		const {
			properties,
		} = this.props;

		let turn = enumElements.wrapper.turn,
				wrapper = {
					id: enumElements.noPrefix(turn.id),
				},
				title = {
					id: enumElements.noPrefix(turn.title.id),
					round: properties.round,
					roundText: `${enumText.turn.round} ${properties.round}`,
				},
				timer = {
					id: enumElements.noPrefix(turn.timer.id),
				},
				word = {
					id: enumElements.noPrefix(turn.word.id),
					controls: {
						classes: enumElements.noPrefix(turn.word.controls.classes),
						correct: {
							id: enumElements.noPrefix(turn.word.controls.correct.id),
						},
						skip: {
							id: enumElements.noPrefix(turn.word.controls.skip.id),
						},
					},
				};

		return (
			<div id={wrapper.id}>
				
				{/* Title */}
				<div id={title.id}>
					<div className="inner dynamic-text" data-min-font-size="22">
						<span className="team-name">{properties.teamName}</span>
						<span className="round-large">
							<span className="separator">-</span>
							<span className="round-counter">{title.roundText}</span>
						</span>
						<span className="round-small">
							<i className="fas fa-stopwatch icon"></i>
							<span className="round-counter">{title.round}</span>
						</span>
					</div>
				</div>

				{/* Timer */}
				<div id={timer.id}>
					<span className="minutes">00</span>
					<span className="separator">:</span>
					<span className="seconds">00</span>
				</div>

				{/* Word */}
				<div id={word.id} className="hidden">
					<span className="word-wrapper dynamic-text" data-size="72" data-min-font-size="22">
						<span></span>
					</span>
					<div className={word.controls.classes}>
						<span id={word.controls.correct.id}>
							<i className="far fa-check-circle"></i>
						</span>
						<span id={word.controls.skip.id}>
							<i className="far fa-times-circle"></i>
						</span>
					</div>
				</div>

			</div>
		);

	}

}