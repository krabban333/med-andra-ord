'use strict';

// Import our modules and classes
import React from 'react';

export default class Card extends React.Component {

	render() {

		// Component properties
		const {
			title,
		} = this.props;

		return (
			<div className="card no-padding-bottom">
				<div className="card-content">
					<div className="inner">

						{(title) && ([
							<h2 key="0" className="card-title">{title}</h2>
						])}

						{this.props.children}

					</div>
				</div>
			</div>
		);

	}

}