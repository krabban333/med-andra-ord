'use strict';

// Import our modules and classes
import React from 'react';
import enumElements from '../../utilities/enum.elements.js';
import enumText from '../../utilities/enum.text.js';

// React components
import PageTitle from '../page-title/page-title';

export default class GameHelp extends React.Component {

	render() {

		let wrapper = {
					id: enumElements.noPrefix(enumElements.wrapper.gameHelp.id),
				},
				text = enumText.gameHelp;

		return (
			<div id={wrapper.id}>

				{/* Page title */}
				<PageTitle title={text.title} />

			{/* Loop through and output all of our paragraphs */}
				{text.paragraphs.map((option, index) => {

					return <p key={index}>{option}</p>;

				})}

			</div>
		);

	}

}