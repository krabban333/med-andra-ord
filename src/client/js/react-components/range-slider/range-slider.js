'use strict';

// Import our modules and classes
import React from 'react';

export default class RangeSlider extends React.Component {

	// On render finish
	componentDidMount() {

		// All range input elements
		let rangeArray = document.querySelectorAll('input[type=range]');

		// Reinitialize range elements
		// This has to be done for the range thumb to work
		M.Range.init(rangeArray);

	}

	render() {

		// Component properties
		const {
			title,
			id,
			extraClasses,
			min,
			max,
			step,
			defaultValue,
		} = this.props;

		let classes = `range-field ${extraClasses}`;

		return (
			<div id={id} className="range-slider">
				{(title) && ([
					<h2 key="0">{title}</h2>
				])}
				<div className={classes}>
					<input type="range" min={min} max={max} defaultValue={defaultValue} step={step} />
				</div>
			</div>
		); 

	}

}

// Default values
RangeSlider.defaultProps = {
	min: 0,
	max: 10,
	step: 1,
}