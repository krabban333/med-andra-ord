'use strict';

// App strings
let applicationData = {
	user: {
		authenticated: false,
		username: '',
		capabilities: {}
	},
	mute: false,
	fullscreen: false,
	gameStarted: false,
	game: {
		settings: {
			teams: [],
			roundTime: 30,
			gameType: 1,
			rounds: 3,
			points: 10,
			wordList: [],
		}
	}
}

export default applicationData;