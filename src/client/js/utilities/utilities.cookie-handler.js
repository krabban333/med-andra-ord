'use strict';

// Import our modules and classes
import cookies from 'js-cookie';

// Wrapper for cookie functions
const cookieHandler = {

	// Set new cookie
	setCookie(name, value, ttl = 365) {

		cookies.set(
			name, 
			value, 
			{
				expires: ttl
			}
		);

	},

	// Get cookie
	getCookie(cookie) {

		return cookies.get(cookie);

	},

	// Remove cookie
	deleteCookie(cookie) {

		cookies.remove(cookie);

	},

}

export default cookieHandler;