'use strict';

// Import our modules and classes
import $ from 'jquery';
import M from 'materialize-css';
import applicationData from './utilities.application-data.js';

// Wrapper for all utility functions
const utility = {

	// Check if object is empty
	isObjectEmpty(object) {

		return (Object.keys(object).length === 0 && object.constructor === Object);

	},

	// Check if undefined, null or whitespace
	isSet(value) {

		return (value !== undefined && value !== null && value.toString().trim() !== '' ? true : false);

	},

	// Removes excessive and preceding whitespaces
	trimWhitespaces(string) {

		return string.trimStart().replace(/\s+/g, ' ');

	},

	// Run CSS3 animation again
	refreshAnimation(targetElement, animation) {

		let $element = $(targetElement);

		$element.removeClass(animation);
		void $element[0].offsetWidth;
		$element.addClass(animation);

	},

	// Activate or deactivate toolbar button
	toggleToolbarBtn(targetElement, active) {

		let $element = $(targetElement);

		(active ? $element.removeClass('disabled') : $element.addClass('disabled'));

	},

	// Handle keyevent for a content editable field
	contentEditableKeyevent(event, element, maxLength = 18, exceptionKey, exceptionCallback) {

		let $element = $(element),
				text = $element.text(),
				selection = window.getSelection(),
				hasSelection = false,
				specialKeys = [];

		// Special keys
		specialKeys = [
			8,  // Backspace
			9,  // Tab
			13, // Enter
			16, // Shift
			17, // Ctrl
			18, // Alt
			37, // Left arrow
			39  // Right arrow
		];

		// Is a callback set for a specific key event
		if(
			exceptionKey !== undefined 
			&& 
			exceptionCallback !== undefined 
			&& 
			event.which === exceptionKey
		) {

			// Run callback
			exceptionCallback();

			return false;

		}

		// Is the key event a special key
		if(specialKeys.indexOf(event.which) !== -1) {

			return false;

		}

		// Check if user has selection
		if(selection) {

			hasSelection = !!selection.toString();

		}

		// Prevent input if maxlength is reached and user has no selection
		if(text.length >= maxLength && !hasSelection) {

			event.preventDefault();

			return false;

		}


	},

	// Place caret at end in input element
	placeCaretAtEnd(element) {

		element.focus();

		if (typeof window.getSelection != 'undefined' && typeof document.createRange != 'undefined') {

			let range = document.createRange(),
					selection = window.getSelection();

			range.selectNodeContents(element);
			range.collapse(false);

			selection.removeAllRanges();
			selection.addRange(range);

		} else if (typeof document.body.createTextRange != "undefined") {

			let textRange = document.body.createTextRange();

			textRange.moveToElementText(element);
			textRange.collapse(false);
			textRange.select();

		}
	},

	// Post data to server
	ajaxPOST(url, data = '', type = 'json') {

		return $.ajax({
			url: url,
			data: data,
			dataType: type,
			type: 'POST',
		});

	},

	// Calculate rangeslider thumb offset
	setRangesliderThumb() {

		$.each(
			$('.range-field.display-thumb'), 
			(index, element) => {

				let $element = $(element),
						$input = $element.find('input[type=range]'),
						$thumb = $element.find('.thumb'),
						rangeInstance = M.Range.getInstance($input),
						offsetLeft,
						value = $input.val();

				// Set correct value
				$thumb.find('.value').text(value);

				// Calculate offset
				offsetLeft = rangeInstance._calcRangeOffset();

				// Set offset
				$thumb.css('left', offsetLeft + 'px');

		});

	},

	// Checks if text is overflowing, then shrinks font-size until it fits inside parent
	// Make sure that parent element is display block or similar
	fitText(parents = $('.dynamic-text'), elements = undefined) {

		let $parents = (parents instanceof $ ? parents : $(parents));

		// Function to calculate the combined width of child elements
		const getChildrenWidth = () => {

			let width = 0;

			// Loop through each element
			elements.each((_, element) => {

				// Is the element a jQuery object
				let $element = (element instanceof $ ? element : $(element));

				// Sum width of elements
				width += $element.outerWidth();

			});

			return width;

		}

		parents.each((_, element) => {

			let $parent = (element instanceof $ ? element : $(element)),
					transitionState = $parent.css('transition'),
					minFontSize = parseInt($parent.data('min-font-size')),
					fontSize = parseInt($parent.css('fontSize').slice(0, -2)),
					parentWidth = $parent.width();

			// Is a custom array of elements set, else get all child elements of parent
			elements = (elements === undefined ? $parent.children() : elements);

			// Disable any transitions on parent element
			$parent.css({
				transition: 'none',
			});

			// Minimize font-size while child elements are overflowing
			while(parentWidth < getChildrenWidth()) {

				// Subtract font-size by one
				fontSize--;

				// Is font-size smaller than minimum allowed size
				if(fontSize < minFontSize) {

					// Add class to parent
					$parent.addClass('text-overflow');

					// And break loop
					break;

				}

				// Set font-size
				$parent.css({
					fontSize: `${fontSize}px`,
				});

			}

			// Enable any transitions on parent element
			$parent.css({
				transition: transitionState,
			});

		});

	},

	// Fisher–Yates shuffle of array
	// stackoverflow.com/a/6274381
	shuffleArray(array) {
		
		for (let i = array.length - 1; i > 0; i--) {

			const j = Math.floor(Math.random() * (i + 1));

			[array[i], array[j]] = [array[j], array[i]];

		}

		return array;

	},

	// Sort array in descending order
	sortScore(array) {

		array.sort((a, b) => {

			return b.score - a.score;

		});

	},

	// Assign gold, silver and bronze to teams
	setPlacings(array) {

		let firstPlace = 0,
				secondPlace = 0,
				thirdPlace = 0;

		for(let i = 0; i <= array.length - 1; i++) {

			let team = array[i];

			// Assign gold icon
			if(team.score >= firstPlace) {

				firstPlace = team.score;
				array[i].icon = 'gold';

			// Assign silver icon
			} else if(team.score >= secondPlace) {

				secondPlace = team.score;
				array[i].icon = 'silver';

			// Assign bronze icon
			} else if(team.score >= thirdPlace) {

				thirdPlace = team.score;
				array[i].icon = 'bronze';

			// Hide all other icons
			} else {

				array[i].icon = 'hidden';

			}

		}

	}

}

export default utility;