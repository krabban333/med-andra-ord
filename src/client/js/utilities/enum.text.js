'use strict';

// App strings
const enumText = {
	header: {
		title: 'Med Andra Ord',
	},
	mainMenu: {
		optNewGame: 'Starta spel',
		optHelp: 'Hur spelar man',
		optSuggestWord: 'Föreslå ord'
	},
	dropdownMenu: {
		optLogin: 'Logga in',
		optLogout: 'Logga ut',
		optEditWords: 'Redigera ordlista',
		optUserSettings: 'Inställningar',
		optFeedback: 'Skicka feedback'
	},
	gameOptions: {
		title: 'Nytt spel',
		teamSlider: {
			title: 'Antal lag',
		},
		teamEditor: {
			title: 'Ändra lagnamn',
			team: 'Lag',
		},
		roundTime: {
			title: 'Tid per runda',
		},
		gameType: {
			title: 'Typ av spel',
			optDefault: {
				text: 'Välj speltyp'
			},
			optRounds: {
				text: 'Rundor',
				description: 'Högsta poäng efter antal rundor vinner',
			},
			optPoints: {
				text: 'Poäng',
				description: 'Först till antal poäng vinner',
			},
			optMixed: {
				text: 'Rundor & poäng',
				description: 'Laget som först tar sig till antal poäng eller har högsta poäng efter antal rundor vinner',
			},
		},
		numRounds: {
			title: 'Antal rundor',
		},
		numPoints: {
			title: 'Antal poäng',
		},
		startGame: {
			title: 'Starta spel',
		},
	},
	gameHelp: {
		title: 'Spelregler',
		paragraphs: [
			'Börja med att dela upp er i lag om två. Spelet går ut på att den ena lagkamraten försöker förklara ordet som visas utan att säga själva ordet. Använd synonymer, motsatser, associationer och andra ledtrådar för att få lagkamraten att förstå vilket ord det gäller.',
			'Gissar ni rätt så trycker du på den gröna knappen, vill du passa tryck på den röda. Efter varje runda byter ni person som har i uppgift att beskriva ordet.',
			'Det är inte tillåtet att använda delar av ett ord för att beskriva ordet, t.ex. "ost" när ordet är "osthyvel". Man får ej heller bokstavera ordet eller säga motsvarigheten på ett annat språk. Gör man fel så passar man till nästa ord genom att trycka på den röda knappen.',
			'Ni ställer själva in hur många lag, tid per runda och speltyp. Skicka gärna in feedback genom att klicka på kugghjulet i nedre högra hörn.',
			'Lycka till!',
		],
	},
	preTurn: {
		title: 'Nästa lag:',
	},
	turn: {
		round: 'Runda',
	},
	score: {
		nextRound: 'Nästa runda',
		back: 'Tillbaka',
	},
	end: {
		buttons: {
			score: 'Visa poäng',
			newGame: 'Nytt spel',
			mainMenu: 'Avsluta',
		},
	},
	modal: {
		cancel: 'Avbryt',
		login: {
			title: 'Logga in',
			username: 'Användarnamn',
			password: 'Lösenord',
			submit: 'Logga in',
		},
		logout: {
			title: 'Vill du logga ut?',
			submit: 'Logga ut',
		},
		confirmEndgame: {
			title: 'Vill du avsluta spelet?',
			submit: 'Avsluta',
		},
	},
	input: {
		empty: 'Fyll i',
		userDenied: 'Felaktigt användarnamn eller lösenord',
		selectGametype: 'Du måste välja en speltyp',
	}
}

export default enumText;