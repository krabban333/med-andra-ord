'use strict';

// CSS classes and ID
const enumElements = {
	cookies: {
		gameSettings: 'game-settings',
	},
	preloader: {
		id: '#preloader',
	},
	modal: {
		classes: '.swal2-container',
		input: {
			modalUsername: {
				id: '#m-username',
			},
			modalPassword: {
				id: '#m-password',
			}
		},
	},
	header: {
		id: '#header-main',
		toolbar: {
			id: '#toolbar',
			optBack: {
				id: '#opt-back',
			},
			optMute: {
				id: '#opt-mute',
			},
			optFullscreen: {
				id: '#opt-fullscreen',
			},
		},
	},
	wrapper: {
		id: '#wrapper-main',
		menuMain: {
			id: '#menu-main',
			optNewGame: {
				id: '#opt-newgame',
			},
			optHelp: {
				id: '#opt-help',
			},
			optSuggestWord: {
				id: '#opt-suggestword',
			},
		},
		gameOptions: {
			id: '#game-options',
			teamsSlider: {
				id: '#teams-slider',
			},
			teamEditor: {
				id: '#team-editor',
				teamList: {
					classes: '.team-list',
				},
			},
			roundClock: {
				id: '#time-clock',
				minutes: {
					classes: '.minutes',
				},
				seconds: {
					classes: '.seconds',
				},
				controls: {
					classes: '.controls',
				}
			},
			gameType: {
				id: '#game-type',
				help: {
					icon: {
						id: '#gametype-help-icon',
					},
					infoBox: {
						id: '#gametype-help-info',
					}
				},
				optRounds: {
					id: '#opt-rounds',
				},
				optPoints: {
					id: '#opt-points',
				},
				optMixed: {
					id: '#opt-mixed',
				},
			},
			numRounds: {
				id: '#num-rounds',
			},
			numPoints: {
				id: '#num-points',
			},
			startGame: {
				id: '#start-game',
			}
		},
		gameHelp: {
			id: '#game-help',
		},
		preTurn: {
			id: '#pre-turn',
			teamName: {
				classes: '.team-name',
			},
			startTurnButton: {
				id: '#start-turn',
			},
		},
		turn: {
			id: '#turn',
			title: {
				id: '#turn-title',
			},
			timer: {
				id: '#turn-timer',
			},
			word: {
				id: '#word',
				controls: {
					classes: '.controls',
					correct: {
						id: '#word-correct',
					},
					skip: {
						id: '#word-skip',
					},
				},
			},
		},
		score: {
			id: '#score',
			display: {
				id: '#score-display',
			},
			nextRound: {
				id: '#btn-next-round',
			},
			back: {
				id: '#btn-back',
			},
		},
		end: {
			id: '#end-screen',
			buttons: {
				score: {
					id: '#btn-show-score',
				},
				newGame: {
					id: '#btn-new-game',
				},
				mainMenu: {
					id: '#btn-main-menu',
				},
			},
		},
	},
	footer: {
		id: '#footer-main',
		dropdownMenu: {
			classes: '.dropdown-menu',
			optLogin: {
				id: '#opt-login',
			},
			optLogout: {
				id: '#opt-logout',
			},
			optEditWords: {
				id: '#opt-editwords',
			},
			optUserSettings: {
				id: '#opt-usersettings',
			},
			optFeedback: {
				id:'#opt-feedback',
			},
		},
	},

	// Remove prefix
	noPrefix: (selector) => {

		return selector.substr(1);

	}

}

export default enumElements;