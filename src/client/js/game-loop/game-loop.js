'use strict';

// Import our modules and classes
import $ from 'jquery';
import enumElements from '../utilities/enum.elements.js';
import applicationData from '../utilities/utilities.application-data.js';
import utility from '../utilities/utilities.js';

export default class GameLoop {

	constructor(contentHandler, mediaHandler) {

		// Content handler instance
		this.content = contentHandler;

		// Media handler
		this.media = mediaHandler;

		// Game varables
		this.settings;
		this.teamQueue;
		this.currentTeam;
		this.round;
		this.countdownTime = 3;
		this.intervalTimer;
		this.wordList = [];
		this.lastWord = undefined;
		this.wordTextSize;
		this.endAudioHasPlayed;

	}

	// Start a new game
	newGame() {

		// Assign settings
		this.settings = applicationData.game.settings;

		// Set round variable
		this.round = 0;

		this.endAudioHasPlayed = false;

		// Get word list
		utility.ajaxPOST(
			'/getwordlist',
		)
		.then((response) => {

			// Set the game to ongoing
			applicationData.gameStarted = true;

			// Assign word list
			applicationData.game.settings.wordList = response;

			// Start new round
			this.newRound();

		})
		.catch((err) => console.error('New game: Could not retrieve word list. ', err.responseText));

	}

	// Start the main gameplay phase
	startTurn() {

		let properties = {
			teamName: this.currentTeam.name,
			round: this.round,
		};

		// Display the turn screen
		this.content.renderContent(
			enumElements.wrapper.id,
			'Turn',
			properties,
			() => {

				let $wrapper = $(enumElements.wrapper.turn.id),
						$roundLarge = $wrapper.find('.round-large');

				// Set width for css animation to work
				$roundLarge.css({
					width: $roundLarge.width()
				});

				// Start countdown
				this._setTurnTimer(
					this.countdownTime,
					true,
					400,
					() => {

						// Animate the title
						$wrapper.addClass('anim-start');

						// Get new word
						this._newWord();

						// Start turn timer
						this._setTurnTimer(
							this.settings.roundTime,
							false,
							1000,
							() => {

								// Start a new turn
								this._newTurn();

							}
						);

					}
				);

			}
		);

	}

	// Add point and display new word
	correctWord() {

		// Get the correct team
		let team = this.settings.teams[$.inArray(this.currentTeam, this.settings.teams)];

		// Add to score
		team.score++;

		// Display new word
		this._newWord();

	}

	// Display new word
	skipWord() {

		// Display new word
		this._newWord();

	}

	// Start a new round
	newRound() {

		// Set team queue
		this.teamQueue = this.settings.teams.slice();

		// Increment round variable
		this.round++;

		// Start a new turn
		this._newTurn();

	}

	// Display the score screen
	displayScoreScreen(gameOver) {

		let properties = {
			round: this.round,
			gameOver: gameOver,
		};

		// Display content
		this.content.renderContent(
			enumElements.wrapper.id,
			'Score',
			properties
		);

	}

	// Display the end of game screen
	displayEndScreen() {

		if(!this.endAudioHasPlayed) {

			this.endAudioHasPlayed = true;

			this.media.playAudio('game_end');

		}

		// Display content
		this.content.renderContent(
			enumElements.wrapper.id,
			'End'
		);

	}

	// Resets the game to it's initial state
	resetGame() {

		applicationData.gameStarted = 0;

		this.teamQueue = [];
		this.currentTeam = undefined;
		this.round = 0;
		this.lastWord = undefined;
		this.endAudioHasPlayed = false;

		clearInterval(this.intervalTimer);

	}

	// Start a new turn
	_newTurn() {

		// Clear any intervals
		clearInterval(this.intervalTimer);

		// Are there any teams that haven't had a turn
		if(this.teamQueue.length > 0) {

			// Get the first team in queue
			this.currentTeam = this.teamQueue.shift();

			// Show the pre turn screen
			this._displayPreTurnScreen(this.currentTeam.name);

		// Each team have had a turn
		} else {

			// Check if the end of the game has been reached
			this._endGameCheck();

		}

	}

	// Display new word
	_newWord() {

		let $word = $(enumElements.wrapper.turn.word.id),
				$wordInner = $word.children('.word-wrapper'),
				$wordWrapper = $wordInner.children('span'),
				currentWord = undefined;

		// Copy and shuffle word list
		const shuffleAndPick = () => {

			this.wordList = utility.shuffleArray(
				this.settings.wordList.slice()
			);

			pick();

		}

		// Get the first word from our shuffled array
		const pick = () => {

			currentWord = this.wordList.shift();
			currentWord = currentWord.word;

		}

		// Is the word list empty
		if(this.wordList.length <= 0) {

			shuffleAndPick();

			// Make sure the new word is not the same as the last word
			while(this.settings.wordList.length > 1 && this.lastWord == currentWord) {

				shuffleAndPick();

			}

		} else {

			pick();

		}

		// Set current word as last word
		this.lastWord = currentWord;

		// Set word
		$wordWrapper.text(currentWord);

		// Check if word is displayed
		if($word.hasClass('hidden')) {

			// Add animation class
			$word.removeClass('hidden');

			// Save original font size
			this.wordTextSize = $wordInner.data('size');

		} else {

			// Animate new word
			utility.refreshAnimation(
				$wordInner[0],
				'fade-in'
			);

		}

		// Reset font size
		$wordInner.css({fontSize: this.wordTextSize})
		.removeClass('text-overflow');

		// Fit text
		utility.fitText($wordInner);


	}

	// Display the pre turn screen
	_displayPreTurnScreen() {

		let properties = {
			teamName: this.currentTeam.name,
		};

		this.media.playAudio(
			'turn_end',
			0.2
		);

		// Display content
		this.content.renderContent(
			enumElements.wrapper.id,
			'PreTurn',
			properties
		);

	}

	// Sets the turn countdown timer
	_setTurnTimer(time, audio = false, endTime = 400, callback = undefined) {

		let $timer = $(enumElements.wrapper.turn.timer.id),
				$minutes = $timer.find('.minutes'),
				$seconds = $timer.find('.seconds'),
				intervalTimer,
				minutes,
				seconds;

		const setTimer = () => {

			// Calculate minutes and seconds
			minutes = Math.floor(time / 60);
			seconds = time % 60;

			// Setup timer
			$minutes.text((`0${minutes}`).slice(-2));
			$seconds.text((`0${seconds}`).slice(-2));

		}

		const tick = (type) => {

			// Should audio be played on every tick
			if(audio) {

				this.media.playAudio(type);

			}

		}

		// Set correct timer values
		setTimer();

		// Stop interval
		clearInterval(this.intervalTimer);
		
		tick('tick');

		// Set interval of one second
		this.intervalTimer = setInterval(() => {

			// Decrease time
			time--;

			// Set correct timer values
			setTimer();

			// Is the time less then zero
			if(time <= 0) {

				tick('tick_end');

				// Stop interval
				clearInterval(this.intervalTimer);

				setTimeout(() => {

					// Run callback if set
					(typeof callback === 'function' && callback());

				}, endTime);

			} else {

				tick('tick');

			}

		}, 1000);

	}

	// Check if the end of the game has been reached
	_endGameCheck() {

		let isEnd = false;

		// Select game type
		switch(this.settings.gameType) {

			// Game type: Rounds
			case 1:

				// Check if all rounds have been played
				isEnd = (this.round >= this.settings.rounds);

				break;

			// Game type: Points
			case 2:
				break;

			// Game type: Mixed
			case 3:
				break;

			default:

				console.error('New round: selected game type does not exist');

				break;

		}

		// Is the game over
		if(isEnd) {

			// Show end screen
			this.displayEndScreen();

		} else {

			// Display the score
			this.displayScoreScreen();

		}
		
	}

}