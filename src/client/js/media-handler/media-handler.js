'use strict';

// Import our modules and classes
import applicationData from '../utilities/utilities.application-data.js';

export default class MediaHandler {

	constructor() {

		// All available audio
		this.audio = {
			correct: new Audio('./assets/audio/correct.mp3'),
			skip: new Audio('./assets/audio/skip.mp3'),
			tick: new Audio('./assets/audio/tick.mp3'),
			tick_end: new Audio('./assets/audio/tick_end.mp3'),
			click: new Audio('./assets/audio/click.mp3'),
			click_alt: new Audio('./assets/audio/click_alt.mp3'),
			range_change: new Audio('./assets/audio/range_change.mp3'),
			turn_end: new Audio('./assets/audio/turn_end.mp3'),
			game_end: new Audio('./assets/audio/game_end.mp3'),
		}

	}

	// Play an audio file
	playAudio(type, volume = 1, loop = false) {

		let audio = this.audio[type];

		if(!applicationData.mute) {

			// Should the audio loop
			audio.loop = loop;

			// Set volume
			audio.volume = volume;

			// Play audio
			audio.currentTime = 0;
			audio.play();

		}

	}

	// Check if all audio has loaded
	_audioReady() {

		let state = true;

		// Loop through all audio
		for(let audio in this.audio) {

			// Is the audio not loaded
			if(this.audio[audio].readyState !== 4) {

				state = false;

			}

		}

		return state;

	}

}