'use strict';

// Import our modules and classes
import applicationData from '../utilities/utilities.application-data.js';
import utility from '../utilities/utilities.js';

// Wrapper for all utility functions
const session = {

	// Get session from server
	get() {

		return new Promise((resolve, reject) => {

			utility.ajaxPOST('/getsession')
			.done((response) => {

				applicationData.user.authenticated = (response.authenticated ? true : false);
				applicationData.user.username = (response.username ? response.username : '');
				applicationData.user.capabilities = (response.capabilities ? response.capabilities : {});

				resolve();

			})
			.fail((err) => {

				console.error('Could not get session:', err);

				reject();

			});

		});

	},

	// Destroy current user session
	destroy() {

		applicationData.user.authenticated = false;
		applicationData.user.username = '';
		applicationData.user.capabilities = {};

		return new Promise((resolve) => {

			utility.ajaxPOST('/destroysession')
			.always(() => {

				resolve();

			});

		});

	}

}

export default session;