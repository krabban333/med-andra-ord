'use strict';

// Fontawesome
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';

// Import our modules and classes
import ContentHandler from './js/content-handler/content-handler.js';
import GameLoop from './js/game-loop/game-loop.js';
import EventListeners from './js/event-listeners/event-listeners.js';
import MediaHandler from './js/media-handler/media-handler.js';

// Required modules
const mediaHandler = new MediaHandler(),
			contentHandler = new ContentHandler(mediaHandler),
			gameLoop = new GameLoop(contentHandler, mediaHandler),
			eventListeners = new EventListeners(contentHandler, gameLoop, mediaHandler);