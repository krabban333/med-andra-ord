'use strict';

// Get our config
import dotenv from 'dotenv';
dotenv.config();

// Import our modules and classes
import Webserver from './webserver/webserver.js';
import DatabaseHandler from './database-handler/database-handler.js';
import EventHandler from './event-handler/event-handler.js';

// Setup classes
const databaseHandler = new DatabaseHandler(),
			eventHandler = new EventHandler(databaseHandler),
			webserver = new Webserver(eventHandler);

// Start webserver
webserver.start();