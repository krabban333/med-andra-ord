'use strict';

// Import our modules and classes
import wordList from './database.structure.wordlist.js';
import utilCrypto from '../../shared/js/util.crypto/util.crypto.js';

// Basic database structure with default values
const databaseStructure = {
	collections: {
		users: [
			{
				username: 'admin',
				password: utilCrypto.hashPassword('admin'),
				capabilities: {
					changeUserSettings: true,
					editWord: true,
					approveWord: true,
					removeWord: true,
				}
			},
		],
		word_list: wordList
	}
}

export default databaseStructure;