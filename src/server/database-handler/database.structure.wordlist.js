'use strict';

// Default word list for quick testing of application
const wordList = [
	{
		word: 'fisk',
		active: true,
	},
	{
		word: 'pizza',
		active: true,
	},
	{
		word: 'hus',
		active: true,
	},
	{
		word: 'julgran',
		active: true,
	},
];

export default wordList;