'use strict';

// Get our config
import dotenv from 'dotenv';
dotenv.config();

// Import our modules and classes
import MongoClient from 'mongodb';
import databaseStructure from './database.structure.js';
import utility from '../utilities/utilities.js'

export default class DatabaseHandler {

	constructor() {

		// Database
		this.db = '';

		// Is the database ready?
		this.databaseReady = false;

		// Connect to database
		this._connect();

	}

	// Insert data
	insertData(collection, data) {

		this.db.collection(collection)
		.insertMany(data)
		.then((response) => {

			if(response.result.ok === 1) {

				utility.log(
					[`Data was successfully inserted into collection ${collection}`],
					'database'
				);

			} else {

				utility.log(
					[`Data could not be inserted into collection ${collection}`],
					'database',
					'error'
				);

			}

		});

	}

	// Get data
	// Data param has to be of type object
	retrieveData(collection, data, fieldSelector = {}) {

		// Use fieldSelector if set, otherwise set it to empty
		fieldSelector = (Object.keys(fieldSelector).length !== 0 && fieldSelector.constructor !== Object ? {} : {projection: fieldSelector});

		return new Promise((resolve, reject) => {

			this.db.collection(collection)
			.find(data, fieldSelector)
			.toArray((err, result) => {

				if(err) {

					reject(err);

				} else if(result.length == 0) {

					reject('data not found');

				}

				resolve(result);

			});

		});

	}

	// Setup database connection
	_connect() {

		MongoClient.connect(
			`mongodb://localhost:${process.env.DATABASE_PORT}`,
			{
				useNewUrlParser: true
			},
			(err, client) => {

				// Connected
				if(!err) {

					utility.log(
						['Connected to database'],
						'database'
					);

					this.db = client.db(process.env.DATABASE_NAME);

					this._verifyStructure();

				// Connection error
				} else {

					utility.log(
						['Could not connect to server:', err],
						'database',
						'error'
					);

				}

			}
		);

	}

	// Checks database structure and creates collections where necessary
	_verifyStructure() {

		let promises;

		// Create promise array
		promises = Object.keys(databaseStructure.collections).map((collection) => {

			return new Promise((resolve, reject) => {

				// Check if collection exists
				this.db.listCollections({
					name: collection
				})
				.next((err, exists) => {

					// Collection does not exist
					if(!exists) {

						let data = [];

						utility.log(
							[`Verify database structure: ${collection} does not exist. Creating new collection ${collection}`],
							'database',
							'error'
						);

						this.insertData(
							collection, 
							databaseStructure.collections[collection]
						);

						reject();

					}
					
					resolve();

				});

			});

		});

		// Resolve our verification promises
		Promise.all(promises)
		.then(() => utility.log(
			['Database structure has been verified'],
			'database'
		))
		.catch((err) => {})
		.finally(() => {

			this.databaseReady = true;

		});

	}

	// Check if data already exists in database
	_verifyData(collection, data) {

		return new Promise((resolve, reject) => {

			this.db.collection(collection)
			.find(data)
			.limit(1)
			.count(true)
			.then((result) => {

				(result && resolve());

				reject();

			})
			.catch((err) => utility.log(
				['Verify data:', err],
				'database',
				'error'
			));

		});

	}

}