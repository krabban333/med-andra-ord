'use strict';

// Get our config
import dotenv from 'dotenv';
dotenv.config();

// Import our modules and classes
import utilCrypto from '../../shared/js/util.crypto/util.crypto.js';
import utility from '../utilities/utilities.js'

export default class EventHandler {

	constructor(databaseHandler) {

		// Our database instance
		this.database = databaseHandler;

	}

	// Validate user and return data if ok
	authorizeUser(user) {

		utility.log(
			['Autorization request for user:', user.username],
			'event'
		);

		return new Promise((resolve, reject) => {

			// Get data from database
			this.database.retrieveData(
				'users',
				{
					username: user.username,
				},
				{
					'_id': 0,
				}
			)
			// Data found
			.then((data) => {

				// We only care about the first result
				data = data[0];

				// Hash the submitted password with the user salt
				let hash = utilCrypto.hashPassword(user.password, data.password.salt).hash;

				// Compare hashes
				if(hash !== data.password.hash) {

					utility.log(
						[`Authorization for user ${user.username} has been denied`],
						'event'
					);

					// And reject if they don't match
					reject();

				} else {

					// We don't need to return the password property
					delete data.password;

					utility.log(
						[`User ${user.username} has been authorized`],
						'event'
					);

					// Return data
					resolve(data);

				}

			})
			// Data not found or error encountered
			.catch((err) => {

				utility.log(
					['Could not retrieve data:', err],
					'database',
					'error'
				);

				reject();

			});

		});

	}

	// Return word list
	getWordList() {

		return new Promise((resolve, reject) => {

			// Get word list
			this.database.retrieveData(
				'word_list',
				{
					active: true,
				},
				{
					_id: 0,
					word: 1,
				}
			)
			.then((result) => {

				// Return list to client
				resolve(result);

			})
			.catch((err) => {

				utility.log(
					['Could not retrieve data:', err],
					'database',
					'error'
				);

				reject();

			});

		});

	}

}