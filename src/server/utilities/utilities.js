'use strict';

// Get our config
import dotenv from 'dotenv';
dotenv.config();

// Import our modules and classes
import os from 'os';

// Wrapper for all utility functions
const utility = {

	// Find matching index in array
	findIndex(array, property, value) {

		let length = array.length;

		while(length--) {

			let arr = array[length];

			if(arr[property] === value) {

				return length;

			}

		}

		return -1;

	},

	// Custom logging
	log(content, namespace = '', type = 'log') {

		let value,
				tag;

		// Identify the logging namespace
		switch(namespace) {

			case 'database':
				value = process.env.LOG_DATABASE;
				tag = '[DATABASE]';
				break;
			
			case 'event':
				value = process.env.LOG_EVENT;
				tag = '[EVENT HANDLER]';
				break;

			case 'webserver':
				value = process.env.LOG_WEBSERVER;
				tag = '[SERVER]';
				break;

			default:
				value = process.env.LOG_ALL;
				tag = '[INFO]';
				break;

		}

		// If all error logging is set to true or namespace is set to true
		if(process.env.LOG_ALL === 'true' || value === 'true') {

			// Identify type of log
			if(type === 'log') {

				console.log(
					`${tag} ${content[0]}`,
					(content[1] != null ? content[1] : '')
				);

			} else if (type === 'error') {

				console.error(
					`${tag} ${content[0]}`,
					(content[1] != null ? content[1] : '')
				);

			} else {

				// Someone messed up
				console.error('[UTILITY] Log: type not found');

			}

		}

	},

	// Generate guid
	guidGenerator() {

		const S4 = function() {

			return (((1+Math.random())*0x10000)|0).toString(16).substring(1);

		};

		return `${S4()}${S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}`;

	},

	// Get public server IP
	serverIP() {

		let interfaces = os.networkInterfaces(),
				addresses = [];

		for(let obj in interfaces) {

			for(let value in interfaces[obj]) {

				let address = interfaces[obj][value];

				if(address.family === 'IPv4' && !address.internal) {

					addresses.push(address.address);

				}

			}

		}

		return addresses;

	},

	// Validates JSON string
	validateJSON(data) {

		try {

			JSON.parse(data);

		} catch(e) {

			return false;

		}

		return true;

	},

	// Loops over object with unknown depth
	traverseObject(object, callback) {

		let keys = Object.keys(object);

		for(let prop in object) {

			let key = keys.find(key => object[key] === object[prop]);

			callback(key, object[prop]);
			
			if(typeof object[prop] === 'object') {

				utility.traverseObject(object[prop], callback);

			}

		}

	},

}

export default utility;