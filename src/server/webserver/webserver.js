'use strict';

// Get our config
import dotenv from 'dotenv';
dotenv.config();

// Import our modules and classes
import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import session from 'express-session';
import passport from 'passport';
import utility from '../utilities/utilities.js'

// Required modules
const app = express(),
			server = http.Server(app);

export default class Webserver {

	constructor(eventHandler) {

		// Our event handler instance
		this.event = eventHandler;

		// Session options
		let sessionOptions = {
			secret: utility.guidGenerator(),
			resave: false,
			saveUninitialized: false,
			cookie: {},
		}

		// Setup our static directories
		app.use(
			express.static(process.env.ROOT)
		);

		// Support parsing of json and x-www-form-urlencode type post data
		app.use(
			bodyParser.json()
		);

		app.use(
			bodyParser.urlencoded({
				extended: true
			})
		);

		// Production settings
		if(process.env.ENVIRONMENT === 'production') {

			// Session settings
			app.set('trust proxy', 1);
			sessionOptions.cookie.secure = true;

		}

		// Setup sessions
		app.use(
			session(sessionOptions)
		);

		// Setup passport for user authentication
		app.use(
			passport.initialize()
		);

		app.use(
			passport.session()
		);

	}

	// Start Webserver
	start() {

		// Start server
		server.listen(
			process.env.PORT, 
			() => utility.log(
				[`Server running on port ${process.env.PORT}`], 
				'webserver'
			)
		);

		// Setup routes
		this._routes();

	}

	// Setup routes
	_routes() {

		// User authorization POST
		app.post('/auth', (req, res) => this.event.authorizeUser(req.body)
			.then((response) => {

				// Set session variables
				req.session.authenticated = true;
				req.session.username = response.username;
				req.session.capabilities = response.capabilities;

				res.send(response);

			})
			.catch((err) => res.send(false))
		);

		// User authorization POST
		app.post('/getsession', (req, res) => res.send(req.session));

		// Destroy the current user session
		app.post('/destroysession', (req, res) => req.session.destroy());

		// Send word list to client
		app.post('/getwordlist', (req, res) => this.event.getWordList()
			.then((response) => res.send(response))
			.catch((err) => res.send(false))
		);

		// Root path
		app.get(
			'/',
			(req, res) => {

				// Redirect to server IP if domain is localhost and we're on a development build
				if(process.env.ENVIRONMENT === 'development' && req.headers.host === `localhost:${process.env.PORT}`) {

					res.redirect(`http://${utility.serverIP()[0]}:${process.env.PORT}${req.url}`);

				// If not, send main file
				} else {

					res.sendFile('main.html', {root: __dirname});

				}

			}
		);

		// 404 path
		app.use(
			(req, res) => res.status(404).send('404 - file not found')
		);

	}

}