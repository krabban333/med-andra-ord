// Get our config
import dotenv from 'dotenv';
dotenv.config();

import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackNotifierPlugin from 'webpack-notifier';
import {sharedPlugins} from './webpack.shared-plugins.js';

/* CLIENTSIDE CONFIG */
export default [

	{
		mode: process.env.ENVIRONMENT,
		devtool: 'source-map',
		entry: [
			'./src/client/main.js', 
			'./src/client/scss/main.scss'
		],
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: './assets/js/main.bundle.js'
		},
		module: {

			rules: [

				// Babel-loader for webpack
				{
					test: /\.(js|jsx)$/,
					exclude: /(node_modules|bower_components)/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env', '@babel/preset-react'],
							plugins:
							[
								[
									'@babel/plugin-transform-runtime',
									{
										regenerator : true,
									}
								],
							],
						}
					}
				},

				// Regular CSS files
				{
					test: /\.css$/,
					use: ExtractTextPlugin.extract({
						use: 'css-loader?importLoaders=1',
					}),
				},

				// SCSS loader for webpack
				{
					test: /\.(sass|scss)$/,
					use: ExtractTextPlugin.extract([
						{
							loader: 'css-loader', 
							options: {
								sourceMap: true
							}
						},
						{
							loader: 'postcss-loader', 
							options: {
								sourceMap: true
							}
						},
						{
							loader: 'sass-loader', 
							options: {
								sourceMap: true
							}
						}
					])
				},

				// Font and SVG loaders
				{
					test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
					loader: 'url-loader?limit=10000&mimetype=application/font-woff',
					options: {
						name: '[name].[ext]',
						outputPath: './assets/css/'
					}
				},
				{
					test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: './assets/css/'
					}
				}

			],
		},
		plugins: [

			// Define where to save the compiled css file
			new ExtractTextPlugin({
				filename: './assets/css/[name].bundle.css',
				allChunks: true,
			}),

			// Send a notification when the build is finished
			new WebpackNotifierPlugin({
				title: 'Client assets'
			}),

		].concat(sharedPlugins),
	}

];