(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/app.js":
/*!***************************!*\
  !*** ./src/server/app.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dotenv */ "dotenv");
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _webserver_webserver_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./webserver/webserver.js */ "./src/server/webserver/webserver.js");
/* harmony import */ var _database_handler_database_handler_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database-handler/database-handler.js */ "./src/server/database-handler/database-handler.js");
/* harmony import */ var _event_handler_event_handler_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./event-handler/event-handler.js */ "./src/server/event-handler/event-handler.js");
 // Get our config


dotenv__WEBPACK_IMPORTED_MODULE_0___default.a.config(); // Import our modules and classes



 // Setup classes

var databaseHandler = new _database_handler_database_handler_js__WEBPACK_IMPORTED_MODULE_2__["default"](),
    eventHandler = new _event_handler_event_handler_js__WEBPACK_IMPORTED_MODULE_3__["default"](databaseHandler),
    webserver = new _webserver_webserver_js__WEBPACK_IMPORTED_MODULE_1__["default"](eventHandler); // Start webserver

webserver.start();

/***/ }),

/***/ "./src/server/database-handler/database-handler.js":
/*!*********************************************************!*\
  !*** ./src/server/database-handler/database-handler.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DatabaseHandler; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "@babel/runtime/helpers/classCallCheck");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "@babel/runtime/helpers/createClass");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dotenv */ "dotenv");
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mongodb */ "mongodb");
/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _database_structure_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./database.structure.js */ "./src/server/database-handler/database.structure.js");
/* harmony import */ var _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utilities/utilities.js */ "./src/server/utilities/utilities.js");
 // Get our config




dotenv__WEBPACK_IMPORTED_MODULE_2___default.a.config(); // Import our modules and classes





var DatabaseHandler =
/*#__PURE__*/
function () {
  function DatabaseHandler() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, DatabaseHandler);

    // Database
    this.db = ''; // Is the database ready?

    this.databaseReady = false; // Connect to database

    this._connect();
  } // Insert data


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(DatabaseHandler, [{
    key: "insertData",
    value: function insertData(collection, data) {
      this.db.collection(collection).insertMany(data).then(function (response) {
        if (response.result.ok === 1) {
          _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(["Data was successfully inserted into collection ".concat(collection)], 'database');
        } else {
          _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(["Data could not be inserted into collection ".concat(collection)], 'database', 'error');
        }
      });
    } // Get data
    // Data param has to be of type object

  }, {
    key: "retrieveData",
    value: function retrieveData(collection, data) {
      var _this = this;

      var fieldSelector = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      // Use fieldSelector if set, otherwise set it to empty
      fieldSelector = Object.keys(fieldSelector).length !== 0 && fieldSelector.constructor !== Object ? {} : {
        projection: fieldSelector
      };
      return new Promise(function (resolve, reject) {
        _this.db.collection(collection).find(data, fieldSelector).toArray(function (err, result) {
          if (err) {
            reject(err);
          } else if (result.length == 0) {
            reject('data not found');
          }

          resolve(result);
        });
      });
    } // Setup database connection

  }, {
    key: "_connect",
    value: function _connect() {
      var _this2 = this;

      mongodb__WEBPACK_IMPORTED_MODULE_3___default.a.connect("mongodb://localhost:".concat(process.env.DATABASE_PORT), {
        useNewUrlParser: true
      }, function (err, client) {
        // Connected
        if (!err) {
          _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(['Connected to database'], 'database');
          _this2.db = client.db(process.env.DATABASE_NAME);

          _this2._verifyStructure(); // Connection error

        } else {
          _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(['Could not connect to server:', err], 'database', 'error');
        }
      });
    } // Checks database structure and creates collections where necessary

  }, {
    key: "_verifyStructure",
    value: function _verifyStructure() {
      var _this3 = this;

      var promises; // Create promise array

      promises = Object.keys(_database_structure_js__WEBPACK_IMPORTED_MODULE_4__["default"].collections).map(function (collection) {
        return new Promise(function (resolve, reject) {
          // Check if collection exists
          _this3.db.listCollections({
            name: collection
          }).next(function (err, exists) {
            // Collection does not exist
            if (!exists) {
              var data = [];
              _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(["Verify database structure: ".concat(collection, " does not exist. Creating new collection ").concat(collection)], 'database', 'error');

              _this3.insertData(collection, _database_structure_js__WEBPACK_IMPORTED_MODULE_4__["default"].collections[collection]);

              reject();
            }

            resolve();
          });
        });
      }); // Resolve our verification promises

      Promise.all(promises).then(function () {
        return _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(['Database structure has been verified'], 'database');
      }).catch(function (err) {}).finally(function () {
        _this3.databaseReady = true;
      });
    } // Check if data already exists in database

  }, {
    key: "_verifyData",
    value: function _verifyData(collection, data) {
      var _this4 = this;

      return new Promise(function (resolve, reject) {
        _this4.db.collection(collection).find(data).limit(1).count(true).then(function (result) {
          result && resolve();
          reject();
        }).catch(function (err) {
          return _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_5__["default"].log(['Verify data:', err], 'database', 'error');
        });
      });
    }
  }]);

  return DatabaseHandler;
}();



/***/ }),

/***/ "./src/server/database-handler/database.structure.js":
/*!***********************************************************!*\
  !*** ./src/server/database-handler/database.structure.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _database_structure_wordlist_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./database.structure.wordlist.js */ "./src/server/database-handler/database.structure.wordlist.js");
/* harmony import */ var _shared_js_util_crypto_util_crypto_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/js/util.crypto/util.crypto.js */ "./src/shared/js/util.crypto/util.crypto.js");
 // Import our modules and classes


 // Basic database structure with default values

var databaseStructure = {
  collections: {
    users: [{
      username: 'admin',
      password: _shared_js_util_crypto_util_crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].hashPassword('admin'),
      capabilities: {
        changeUserSettings: true,
        editWord: true,
        approveWord: true,
        removeWord: true
      }
    }],
    word_list: _database_structure_wordlist_js__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
};
/* harmony default export */ __webpack_exports__["default"] = (databaseStructure);

/***/ }),

/***/ "./src/server/database-handler/database.structure.wordlist.js":
/*!********************************************************************!*\
  !*** ./src/server/database-handler/database.structure.wordlist.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
 // Default word list for quick testing of application

var wordList = [{
  word: 'fisk',
  active: true
}, {
  word: 'pizza',
  active: true
}, {
  word: 'hus',
  active: true
}, {
  word: 'julgran',
  active: true
}];
/* harmony default export */ __webpack_exports__["default"] = (wordList);

/***/ }),

/***/ "./src/server/event-handler/event-handler.js":
/*!***************************************************!*\
  !*** ./src/server/event-handler/event-handler.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EventHandler; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "@babel/runtime/helpers/classCallCheck");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "@babel/runtime/helpers/createClass");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dotenv */ "dotenv");
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _shared_js_util_crypto_util_crypto_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/js/util.crypto/util.crypto.js */ "./src/shared/js/util.crypto/util.crypto.js");
/* harmony import */ var _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utilities/utilities.js */ "./src/server/utilities/utilities.js");
 // Get our config




dotenv__WEBPACK_IMPORTED_MODULE_2___default.a.config(); // Import our modules and classes




var EventHandler =
/*#__PURE__*/
function () {
  function EventHandler(databaseHandler) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, EventHandler);

    // Our database instance
    this.database = databaseHandler;
  } // Validate user and return data if ok


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(EventHandler, [{
    key: "authorizeUser",
    value: function authorizeUser(user) {
      var _this = this;

      _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_4__["default"].log(['Autorization request for user:', user.username], 'event');
      return new Promise(function (resolve, reject) {
        // Get data from database
        _this.database.retrieveData('users', {
          username: user.username
        }, {
          '_id': 0
        }) // Data found
        .then(function (data) {
          // We only care about the first result
          data = data[0]; // Hash the submitted password with the user salt

          var hash = _shared_js_util_crypto_util_crypto_js__WEBPACK_IMPORTED_MODULE_3__["default"].hashPassword(user.password, data.password.salt).hash; // Compare hashes

          if (hash !== data.password.hash) {
            _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_4__["default"].log(["Authorization for user ".concat(user.username, " has been denied")], 'event'); // And reject if they don't match

            reject();
          } else {
            // We don't need to return the password property
            delete data.password;
            _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_4__["default"].log(["User ".concat(user.username, " has been authorized")], 'event'); // Return data

            resolve(data);
          }
        }) // Data not found or error encountered
        .catch(function (err) {
          _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_4__["default"].log(['Could not retrieve data:', err], 'database', 'error');
          reject();
        });
      });
    } // Return word list

  }, {
    key: "getWordList",
    value: function getWordList() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        // Get word list
        _this2.database.retrieveData('word_list', {
          active: true
        }, {
          _id: 0,
          word: 1
        }).then(function (result) {
          // Return list to client
          resolve(result);
        }).catch(function (err) {
          _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_4__["default"].log(['Could not retrieve data:', err], 'database', 'error');
          reject();
        });
      });
    }
  }]);

  return EventHandler;
}();



/***/ }),

/***/ "./src/server/utilities/utilities.js":
/*!*******************************************!*\
  !*** ./src/server/utilities/utilities.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/typeof */ "@babel/runtime/helpers/typeof");
/* harmony import */ var _babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dotenv */ "dotenv");
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var os__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! os */ "os");
/* harmony import */ var os__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(os__WEBPACK_IMPORTED_MODULE_2__);
 // Get our config



dotenv__WEBPACK_IMPORTED_MODULE_1___default.a.config(); // Import our modules and classes

 // Wrapper for all utility functions

var utility = {
  // Find matching index in array
  findIndex: function findIndex(array, property, value) {
    var length = array.length;

    while (length--) {
      var arr = array[length];

      if (arr[property] === value) {
        return length;
      }
    }

    return -1;
  },
  // Custom logging
  log: function log(content) {
    var namespace = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'log';
    var value, tag; // Identify the logging namespace

    switch (namespace) {
      case 'database':
        value = process.env.LOG_DATABASE;
        tag = '[DATABASE]';
        break;

      case 'event':
        value = process.env.LOG_EVENT;
        tag = '[EVENT HANDLER]';
        break;

      case 'webserver':
        value = process.env.LOG_WEBSERVER;
        tag = '[SERVER]';
        break;

      default:
        value = process.env.LOG_ALL;
        tag = '[INFO]';
        break;
    } // If all error logging is set to true or namespace is set to true


    if (process.env.LOG_ALL === 'true' || value === 'true') {
      // Identify type of log
      if (type === 'log') {
        console.log("".concat(tag, " ").concat(content[0]), content[1] != null ? content[1] : '');
      } else if (type === 'error') {
        console.error("".concat(tag, " ").concat(content[0]), content[1] != null ? content[1] : '');
      } else {
        // Someone messed up
        console.error('[UTILITY] Log: type not found');
      }
    }
  },
  // Generate guid
  guidGenerator: function guidGenerator() {
    var S4 = function S4() {
      return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
    };

    return "".concat(S4()).concat(S4(), "-").concat(S4(), "-").concat(S4(), "-").concat(S4(), "-").concat(S4()).concat(S4()).concat(S4());
  },
  // Get public server IP
  serverIP: function serverIP() {
    var interfaces = os__WEBPACK_IMPORTED_MODULE_2___default.a.networkInterfaces(),
        addresses = [];

    for (var obj in interfaces) {
      for (var value in interfaces[obj]) {
        var address = interfaces[obj][value];

        if (address.family === 'IPv4' && !address.internal) {
          addresses.push(address.address);
        }
      }
    }

    return addresses;
  },
  // Validates JSON string
  validateJSON: function validateJSON(data) {
    try {
      JSON.parse(data);
    } catch (e) {
      return false;
    }

    return true;
  },
  // Loops over object with unknown depth
  traverseObject: function traverseObject(object, callback) {
    var keys = Object.keys(object);

    var _loop = function _loop(prop) {
      var key = keys.find(function (key) {
        return object[key] === object[prop];
      });
      callback(key, object[prop]);

      if (_babel_runtime_helpers_typeof__WEBPACK_IMPORTED_MODULE_0___default()(object[prop]) === 'object') {
        utility.traverseObject(object[prop], callback);
      }
    };

    for (var prop in object) {
      _loop(prop);
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (utility);

/***/ }),

/***/ "./src/server/webserver/webserver.js":
/*!*******************************************!*\
  !*** ./src/server/webserver/webserver.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Webserver; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "@babel/runtime/helpers/classCallCheck");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "@babel/runtime/helpers/createClass");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dotenv */ "dotenv");
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! http */ "http");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var express_session__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! express-session */ "express-session");
/* harmony import */ var express_session__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(express_session__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var passport__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! passport */ "passport");
/* harmony import */ var passport__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(passport__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utilities/utilities.js */ "./src/server/utilities/utilities.js");
 // Get our config




dotenv__WEBPACK_IMPORTED_MODULE_2___default.a.config(); // Import our modules and classes






 // Required modules

var app = express__WEBPACK_IMPORTED_MODULE_3___default()(),
    server = http__WEBPACK_IMPORTED_MODULE_4___default.a.Server(app);

var Webserver =
/*#__PURE__*/
function () {
  function Webserver(eventHandler) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Webserver);

    // Our event handler instance
    this.event = eventHandler; // Session options

    var sessionOptions = {
      secret: _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_8__["default"].guidGenerator(),
      resave: false,
      saveUninitialized: false,
      cookie: {} // Setup our static directories

    };
    app.use(express__WEBPACK_IMPORTED_MODULE_3___default.a.static(process.env.ROOT)); // Support parsing of json and x-www-form-urlencode type post data

    app.use(body_parser__WEBPACK_IMPORTED_MODULE_5___default.a.json());
    app.use(body_parser__WEBPACK_IMPORTED_MODULE_5___default.a.urlencoded({
      extended: true
    })); // Production settings

    if (process.env.ENVIRONMENT === 'production') {
      // Session settings
      app.set('trust proxy', 1);
      sessionOptions.cookie.secure = true;
    } // Setup sessions


    app.use(express_session__WEBPACK_IMPORTED_MODULE_6___default()(sessionOptions)); // Setup passport for user authentication

    app.use(passport__WEBPACK_IMPORTED_MODULE_7___default.a.initialize());
    app.use(passport__WEBPACK_IMPORTED_MODULE_7___default.a.session());
  } // Start Webserver


  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Webserver, [{
    key: "start",
    value: function start() {
      // Start server
      server.listen(process.env.PORT, function () {
        return _utilities_utilities_js__WEBPACK_IMPORTED_MODULE_8__["default"].log(["Server running on port ".concat(process.env.PORT)], 'webserver');
      }); // Setup routes

      this._routes();
    } // Setup routes

  }, {
    key: "_routes",
    value: function _routes() {
      var _this = this;

      // User authorization POST
      app.post('/auth', function (req, res) {
        return _this.event.authorizeUser(req.body).then(function (response) {
          // Set session variables
          req.session.authenticated = true;
          req.session.username = response.username;
          req.session.capabilities = response.capabilities;
          res.send(response);
        }).catch(function (err) {
          return res.send(false);
        });
      }); // User authorization POST

      app.post('/getsession', function (req, res) {
        return res.send(req.session);
      }); // Destroy the current user session

      app.post('/destroysession', function (req, res) {
        return req.session.destroy();
      }); // Send word list to client

      app.post('/getwordlist', function (req, res) {
        return _this.event.getWordList().then(function (response) {
          return res.send(response);
        }).catch(function (err) {
          return res.send(false);
        });
      }); // Root path

      app.get('/', function (req, res) {
        // Redirect to server IP if domain is localhost and we're on a development build
        if (process.env.ENVIRONMENT === 'development' && req.headers.host === "localhost:".concat(process.env.PORT)) {
          res.redirect("http://".concat(_utilities_utilities_js__WEBPACK_IMPORTED_MODULE_8__["default"].serverIP()[0], ":").concat(process.env.PORT).concat(req.url)); // If not, send main file
        } else {
          res.sendFile('main.html', {
            root: __dirname
          });
        }
      }); // 404 path

      app.use(function (req, res) {
        return res.status(404).send('404 - file not found');
      });
    }
  }]);

  return Webserver;
}();



/***/ }),

/***/ "./src/shared/js/util.crypto/util.crypto.js":
/*!**************************************************!*\
  !*** ./src/shared/js/util.crypto/util.crypto.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto */ "crypto");
/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto__WEBPACK_IMPORTED_MODULE_0__);
 // Import our modules and classes

 // Wrapper for all utility functions

var utilCrypto = {
  // Generate password hash and salt
  hashPassword: function hashPassword(password) {
    var salt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : crypto__WEBPACK_IMPORTED_MODULE_0___default.a.randomBytes(16).toString('hex');
    password = crypto__WEBPACK_IMPORTED_MODULE_0___default.a.pbkdf2Sync(password, salt, 1000, 128, 'sha512').toString('hex');
    return {
      hash: password,
      salt: salt
    };
  }
};
/* harmony default export */ __webpack_exports__["default"] = (utilCrypto);

/***/ }),

/***/ 0:
/*!*********************************!*\
  !*** multi ./src/server/app.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./src/server/app.js */"./src/server/app.js");


/***/ }),

/***/ "@babel/runtime/helpers/classCallCheck":
/*!********************************************************!*\
  !*** external "@babel/runtime/helpers/classCallCheck" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/classCallCheck");

/***/ }),

/***/ "@babel/runtime/helpers/createClass":
/*!*****************************************************!*\
  !*** external "@babel/runtime/helpers/createClass" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/createClass");

/***/ }),

/***/ "@babel/runtime/helpers/typeof":
/*!************************************************!*\
  !*** external "@babel/runtime/helpers/typeof" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/typeof");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "crypto":
/*!*************************!*\
  !*** external "crypto" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("crypto");

/***/ }),

/***/ "dotenv":
/*!*************************!*\
  !*** external "dotenv" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("dotenv");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "express-session":
/*!**********************************!*\
  !*** external "express-session" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express-session");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongodb");

/***/ }),

/***/ "os":
/*!*********************!*\
  !*** external "os" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),

/***/ "passport":
/*!***************************!*\
  !*** external "passport" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("passport");

/***/ })

/******/ })));
//# sourceMappingURL=app.js.map