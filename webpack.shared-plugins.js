// Get our config
import dotenv from 'dotenv';
dotenv.config();

import webpack from 'webpack';
import CircularDependencyPlugin from 'circular-dependency-plugin';

// Shared plugins
export const sharedPlugins = [
	
	// Get rid of circular dependencies
	new CircularDependencyPlugin({
		exclude: /a\.js|node_modules/,
		failOnError: true,
		allowAsyncCycles: false,
		cwd: process.cwd(),
	}),

	// Make version number available in js files
	new webpack.DefinePlugin({
		VERSION: JSON.stringify(require('./package.json').version)
	})

];